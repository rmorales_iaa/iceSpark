//=============================================================================
//File: stationMeteorology.scala
//=============================================================================
/** It implements a data manager for meterelogy data
 *  @author  Rafael Morales Muñoz
 *  @mail    rmorales.iaa.es
 *  @version 1.0
 *  @date    24 Aug 2016
 *  @history None
 */
//=============================================================================
//=============================================================================
// Package section
//=============================================================================
package iceSpark.data.meteorology.meteorology

//=============================================================================
// System import section
//=============================================================================
import java.util.Date
import java.io.File

import org.apache.spark.sql.{DataFrame, Row, SparkSession}
import org.apache.spark.sql.types._

import org.joda.time.{ DateTime, DateTimeZone }
import org.joda.time.format.DateTimeFormat

//=============================================================================
// User import section
//=============================================================================
import catSpark.configuration.MyConf
import catSpark.database.mongoDB.MyMongoDB
import catSpark.logger.visual.LoggerVisual
import catSpark.spark.dataFrame.MyDataframe
import catSpark.util.MyUtil

//=============================================================================
// Class/Object implementation
//=============================================================================
object StationMeteorology {
  //-------------------------------------------------------------------------
  //Class variables section
  //-------------------------------------------------------------------------
  var log : LoggerVisual = null
  var sparkSession: SparkSession = null

  //-------------------------------------------------------------------------  
  //station list  

  val defaultDateTimeFormat = DateTimeFormat.forPattern("dd/MM/yyyy:hh:mm:ss aa").withZone(DateTimeZone.UTC)
  
  //deception isle. Station Gabriel de Castilla                   
  val dcp = new Station ( MyUtil.ensureEndWithFileSeparator(MyConf.getString( "IceSpark.data.meteorology.deceptionIsle.rootInputPath" )) 
                       ,  MyUtil.ensureEndWithFileSeparator(MyConf.getString( "IceSpark.data.meteorology.deceptionIsle.rootoOutPath" ))
                       ,  defaultDateTimeFormat
                       ,  StationType.Dcp)
                     
  //livingston isle. Station Juan Carlos I
  val lvn = new Station (MyUtil.ensureEndWithFileSeparator(MyConf.getString( "IceSpark.data.meteorology.livingStonIsle.rootInputPath" )) 
                      ,  MyUtil.ensureEndWithFileSeparator(MyConf.getString( "IceSpark.data.meteorology.livingStonIsle.rootoOutPath" ))
                      ,  defaultDateTimeFormat
                      ,  StationType.Lvn)
  
  //Caleta Cierva station
  val ccv = new Station (MyUtil.ensureEndWithFileSeparator(MyConf.getString( "IceSpark.data.meteorology.caletaCiervaStation.rootInputPath" )) 
                      ,  MyUtil.ensureEndWithFileSeparator(MyConf.getString( "IceSpark.data.meteorology.caletaCiervaStation.rootoOutPath" ))
                      ,  defaultDateTimeFormat
                      ,  StationType.Ccv)
  //station list
  val stationList = List( dcp, lvn, ccv )
  
  //-------------------------------------------------------------------------  
  //mongo connection
  var mongoConnection: MyMongoDB = null
 
  //-------------------------------------------------------------------------
  // Code starts
  //-------------------------------------------------------------------------  
  def translateDirectory(_startDate: DateTime, _endDate: DateTime, _mongoConnection: MyMongoDB){

    //set the range date  
    Station.startDate = _startDate.getMillis
    Station.endDate = _endDate.getMillis
    
       //set mongoConnection
    mongoConnection = _mongoConnection
    
    stationList.foreach{ station => station.translateDirectory() }
  }
  
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
} //end of object 'StationMeteorology'
//=============================================================================
// End of 'stationMeteorology.scala' file
//=============================================================================
