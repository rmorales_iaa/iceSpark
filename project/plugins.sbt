//Start of file

//build a fat jar with all dependences. https://github.com/sbt/sbt-assembly
addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.14.5")

//Create a eclipse project. https://github.com/typesafehub/sbteclipse
addSbtPlugin("com.typesafe.sbteclipse" % "sbteclipse-plugin" % "5.2.2")

//Dependence graph tree. https://github.com/jrudolph/sbt-dependency-graph
addSbtPlugin("net.virtual-void" % "sbt-dependency-graph" % "0.8.2")

//generates Scala source from your build definitions in sbt file.  https://github.com/sbt/sbt-buildinfo
addSbtPlugin("com.eed3si9n" % "sbt-buildinfo" % "0.7.0")

//manager of git versioning. https://github.com/sbt/sbt-git
addSbtPlugin("com.typesafe.sbt" % "sbt-git" % "0.9.3")

//jni bridge. https://github.com/jodersky/sbt-jni
addSbtPlugin("ch.jodersky" % "sbt-jni" % "1.2.6")

//End of file
