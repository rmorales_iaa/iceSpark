//=============================================================================
//File: iceSpark.scala
//=============================================================================
/** It implements a toll to manage Gaia data
 *  @author  Rafael Morales Muñoz
 *  @mail    rmorales.iaa.es
 *  @version 1.0
 *  @date    26 Marc 2017
 *  @history None
 */
//=============================================================================
//=============================================================================
// Package section
//=============================================================================
package iceSpark

//=============================================================================
// System import section
//=============================================================================

object Main{
  def main( args: Array[String] ) {

    //build and run
    val app = new IceSpark( args )
  }
}

//=============================================================================
//=============================================================================
import iceSpark.mongoDB.{IceExtensionMDB, MeteoMDB, SeismicMDB, TideMDB }

import java.awt.Color
import java.io.File

import javax.swing.border.{ CompoundBorder, EmptyBorder, LineBorder }
import javax.swing.filechooser.FileNameExtensionFilter
import javax.swing.ImageIcon

import org.apache.commons.io.FilenameUtils
import org.joda.time.{ DateTime, DateTimeZone }
import org.joda.time.format.DateTimeFormat

import scala.io.Source
import scala.swing._

//=============================================================================
// User import section
//=============================================================================
import catSpark.configuration.MyConf
import catSpark.main.MainApp
import catSpark.logger.visual.LoggerVisual
import catSpark.database.mongoDB.MyMongoDB
import catSpark.office.apache.poi.Poi
import catSpark.spark.dataFrame.MyDataframe
import catSpark.spark.MySpark
import catSpark.util.MyUtil

import iceSpark.data.ice.extension.IceExtension
import iceSpark.data.iceberg.position.IcebergPosition
import iceSpark.data.meteorology.meteorology.{ Station, StationMeteorology }
import iceSpark.data.seismic.seismic.SeismicMiniSeed
import iceSpark.data.tide.tide.Tide

import iceSpark.mongoDB.SeismicMDB
import iceSpark.query.Query
import iceSpark.query.seismic.QuerySeismic

//=============================================================================
// Class/Object implementation
//=============================================================================
import MainApp._
object IceSpark{
  
  //-------------------------------------------------------------------------  
  //closing
  var closing = false
  
  //-------------------------------------------------------------------------  
  //Version
  private final val MajorVersion=0
  private final val MinorVersion=0
  private final val Compilation=398

  //-------------------------------------------------------------------------  
  //Spark
  val sp = new MySpark( MainApp.log )
 
  //-------------------------------------------------------------------------  
  //local info
  private val userName = MyConf.getString("IceSpark.userName")
  var mongoDatabaseName = "" 
  var station = ""
  var year = ""  
  
  var signalMinTimeMs:Long = 0
  var signalMaxTimeMs:Long = 0
  var timeSliceRangeInS: Int=0
  
  var bandPassFilterLowHz : Double = 0.0
  var bandPassFilterHighHz : Double = 0.0
  var lowPassFilterHz : Double = 0.0
   
  var surMinAmplitude: Long =0
  var surMinTimeToGlueMs: Long = 0
  var mongoUserRunInfo = "" 

  //mongo  
  var iceExtensionMDB: IceExtensionMDB = null
  var meteoMDB: MeteoMDB = null
  var seismicMDB: SeismicMDB = null
  var tideMDB: TideMDB = null
 
  //-------------------------------------------------------------------------  
  //-------------------------------------------------------------------------  
}

//=============================================================================

import IceSpark._
import java.util.Date
class IceSpark( userArgumentList: Array[String] ) extends MainApp {
  
  //------------------------------------------------------------------------- 
  //GUI
  
  //------------------------------------------------------------------------- 
  //date
  val DateRangeFormat = null 
  var startDate : DateTime = null
  var endDate : DateTime = null
   
  // seismic
  var seismic : QuerySeismic = null
  
  //-------------------------------------------------------------------------
  // Code
  //-------------------------------------------------------------------------  
  
  init
  
  //-------------------------------------------------------------------------
  def loadLastUserSettingList {
    
  }

  //-------------------------------------------------------------------------
  override def setGlobalObject {
        
    super.setGlobalObject
    if (!initSpark) return       //initialize spark
    
    
    //logger
    SandBox.log = log
    SeismicMiniSeed.log = log
    Tide.log = log
    IceExtension.log = log
    Poi.log = log
    Station.log = log
    IcebergPosition.log = log
    Query.log = log
    
    //spark
    SandBox.sp = sp
    
    //spark session
    MyDataframe.sqlContext =  sp.getSparkSession.sqlContext
    SeismicMiniSeed.sparkSession = sp.getSparkSession
    Tide.sparkSession = sp.getSparkSession
    IceExtension.sparkSession = sp.getSparkSession
    Station.sparkSession = sp.getSparkSession
    IcebergPosition.sparkSession = sp.getSparkSession
    MyMongoDB.sparkSession = sp.getSparkSession
    
    //MongoDB
    MyMongoDB.log = log
        
    //iceSpark
    SandBox.main = this    
  }   

  //-------------------------------------------------------------------------
  override def initScheduler = {
    
    val hk = new HK
    scheduler.add("HK", hk.build, periodInS=5,initalDelayInS=1)    
  }

  //-------------------------------------------------------------------------
  override def closeOperation{

    if (sp != null) sp.close    
    super.closeOperation
  }
  
  //-------------------------------------------------------------------------
  def initSpark : Boolean = {
    
    if (!MyConf.getBoolean("Spark.use")) return log.info("Not using Spark capabilities")
    if (sp.initialize("iceSpark")) log.info("Spark initialized")
    true
  }  
  
  //-------------------------------------------------------------------------
  private def parseDateRange {
    
    //Date format
    val DateRangeFormat = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss:SSS").withZone(DateTimeZone.UTC)
    
    startDate =  DateRangeFormat.parseDateTime(MyConf.getString("IceSpark.data.startDate"))
    endDate =    DateRangeFormat.parseDateTime(MyConf.getString("IceSpark.data.endDate"))  
  
    log.info(s"Using start date for parsing data: '$startDate'" );
    log.info(s"Using end date for parsing data:   '$endDate'" );
    log.info(s"Both, start and end date are included")    
  }
  
  //-------------------------------------------------------------------------    
  def translateMiniSeedData(inputMiniSeedRoot: String
    ,  componentToParse: Int = 4) = { //1 for E-component, 2 for N-component, 4 for Z component, and logical combinations (5 for N and E)
    
    seismicMDB.drop  //it is fastest
    
    SeismicMiniSeed.translate(inputMiniSeedRoot 
        ,  station
        ,  year
        ,  seismicMDB
        ,  startDate
        ,  endDate
        ,  componentToParse)              
  }
  
  //-------------------------------------------------------------------------
  def setMongoCollection(){
    
    val prefix =  station + "_"+year 
    iceExtensionMDB = new IceExtensionMDB(mongoDatabaseName, prefix+ "_ice")
    meteoMDB =        new MeteoMDB       (mongoDatabaseName, prefix + "_meteo")
    seismicMDB =      new SeismicMDB     (mongoDatabaseName, prefix + "_seismic")
    tideMDB =         new TideMDB        (mongoDatabaseName, prefix + "_tide") 
  }
  
  //-------------------------------------------------------------------------
  def closeMongoCollection(){
    
    iceExtensionMDB.close
    meteoMDB.close
    seismic.closeMongoConnections
    tideMDB.close 
  }
    
  //------------------------------------------------------------------------- 
  def processTide {
       
    log.info( s"Importing to MongoDB tide data")
    Tide.translateDirectory(startDate, endDate, tideMDB)
  }
    
  //------------------------------------------------------------------------- 
  def processMeteorology {       
    log.info( s"Importing to MongoDB metereology data")
    StationMeteorology.translateDirectory(startDate, endDate, meteoMDB)
  }

  //------------------------------------------------------------------------- 
  def processIceExtension {
    
    val iceExtensionMDB = new IceExtensionMDB(mongoDatabaseName,station+"_"+year+"_ice")
    
    log.info( s"Importing to MongoDB ice extension data")
    IceExtension.translateDirectory(startDate, endDate, iceExtensionMDB)
  }
  
  //-------------------------------------------------------------------------
  import org.apache.spark.sql.{ DataFrame, Row, SQLContext, SparkSession }
  
  private def printMinMaxDF(df: DataFrame, sortColName: String,message: String = ""){
    import org.apache.spark.sql.functions._
  
    println(message)
    
    df.sort(sortColName).show(1)
    df.sort(desc(sortColName)).show(1)    
  }
   //-------------------------------------------------------------------------  
  private def kmeans  {
    
    val l =  List(
       "/media/data2TB/vane/lp_analysis/ccv_2009/ccv_2009_filt_30_1.5-40s_2-15hz_psd_FINAL_JOINED_DATA.csv"
     )
    
    val psdTotalColName = "psdTotal"
    val tideColName = "tide_height[m]"
    
    l.map{ f=>
      val dfRaw = MyDataframe.loadCSV(f).get
      val df = dfRaw.filter(dfRaw(tideColName) > 0)
      
      val r = sp.kmeans(df
        ,  clusterCount = 5
        ,  iteration=1000
        ,  run=1000
        ,  epsilon=0.5
        ,  userColNameList=Seq(tideColName, psdTotalColName))

      r._2.show      
    }
  }
  
  //-------------------------------------------------------------------------  
  private def stats  {
  
    val l =  List(
       "/media/data2TB/vane/lp_analysis/ccv_2009/ccv_2009_filt_30_1.5-40s_2-15hz_psd_FINAL_JOINED_DATA.csv"
    )

    val psdTotalColName = "psdTotal"
    val tideColName = "tide_height[m]"
    
     l.map{ f=>
      val dfRaw = MyDataframe.loadCSV(f).get
      val df = dfRaw.filter(dfRaw(tideColName) > 0)
      
    //  sp.myStat.frequency(df, Seq(psdTotalColName,tideColName))
      sp.myStat.quantile(df, psdTotalColName)
    }
  }
  
  //-------------------------------------------------------------------------
  import org.apache.spark.sql.functions.udf
  
  val aggValue= udf((x: Double) => {
    if (x > 3) x.toDouble
    else x.toDouble
  })
  
  //-------------------------------------------------------------------------  
  private def kNearestNeighbor  {
    
    val l =  List(
       "/media/data2TB/vane/lp_analysis/ccv_2009/ccv_2009_filt_30_1.5-40s_2-15hz_psd_FINAL_JOINED_DATA.csv"
     )
    
    val startTimeColName  = "startTimeMs"
    val psdTotalColName = "psdTotal"
    val tideColName = "tide_height[m]"
    
    l.map{ f=>
      val dfRaw = MyDataframe.loadCSV(f).get
      val df_1 = MyDataframe.takeFirstRowsDataframe( dfRaw.filter(dfRaw(tideColName) > 0), 1000)      
      val df = MyDataframe.setColDataTypeToDouble(df_1, startTimeColName)
          
      val parent = MyUtil.getParentPath(f)+"/"
      val output = parent + MyUtil.getFileNameNoPathNoExtension( f ) + "_K_NEREAST"    
      
      sp.kNearestNeighbor(df
        ,  neighborCount = 5
        ,  scale=10
        
        //,  colNameA= tideColName
        //,  colNameB= psdTotalColName
        //,  colNameC= ""
        
        
        //,  colNameA= startTimeColName
        //,  colNameB= tideColName
        //,  colNameC= ""
        
        //,  colNameA= startTimeColName
        //,  colNameB= psdTotalColName
        //,  colNameC= ""
        
        //,  colNameA= tideColName
        //,  colNameB= startTimeColName
        //,  colNameC= psdTotalColName
        
        ,  colNameA= startTimeColName
        ,  colNameB= psdTotalColName
        ,  colNameC= tideColName
        
        ,  output) 
    }
  }
  
  //-------------------------------------------------------------------------    
  import org.apache.spark.sql.functions.round
  
  //-------------------------------------------------------------------------
  private def aggregateAndCountSignalRaw(df: DataFrame, message: String)  {
    
    val tideColName = "tide_height[m]"
    val tideAggColName = "tide_height_m"    
    val r = MyDataframe.setColDataTypeToLong(df, tideAggColName).sort(tideAggColName)
    log.info(message)
    sp.myStat.aggregateAndCount(r, Seq(tideAggColName), "lp_count")
  }
  
  //-------------------------------------------------------------------------
  private def aggregateAndCountSignal(fileList: List[String])  {
  
    val psdTotalColName = "psdTotal"
    val tideColName = "tide_height[m]"
    val tideAggColName = "tide_height_m"
    val idColName = "lp_id"
    
    fileList.map{ f=>
      val station = MyUtil.getParentPath(f).split("/").last
      val dfRaw = MyDataframe.loadCSV(f).get
      val df = dfRaw.select(idColName, psdTotalColName, tideColName)
        .filter(dfRaw(tideColName) > 0)
      
      aggregateAndCountSignalRaw(df.                                        withColumn(tideAggColName, round(dfRaw(tideColName))), station + " ALL") 
      aggregateAndCountSignalRaw(df.where(df(idColName) =!=  "NO_LP").      withColumn(tideAggColName, round(dfRaw(tideColName))), station + " != NO_LP")        
      aggregateAndCountSignalRaw(df.where(df(idColName) ===  "LP_HIGH_FEQ").withColumn(tideAggColName, round(dfRaw(tideColName))), station + " ==LP_HIGH_FEQ")
      aggregateAndCountSignalRaw(df.where(df(idColName) ===  "LP_LOW_FEQ"). withColumn(tideAggColName, round(dfRaw(tideColName))), station + " ==LP_LOW_FEQ")
      aggregateAndCountSignalRaw(df.where(df(idColName) ===  "NO_LP").      withColumn(tideAggColName, round(dfRaw(tideColName))), station + " ==NO_LP")        
    }
  }
  
  //-------------------------------------------------------------------------
  private def aggregateAndCountIceExtensionRaw(df: DataFrame, message: String)  {
    
    val iceExtensionColName = "extension[Km3]"
    val iceExtensionAggColName = "extension_Km3"  
    val r = MyDataframe.setColDataTypeToLong(df, iceExtensionAggColName).sort(iceExtensionAggColName)
    log.info(message)
    sp.myStat.aggregateAndCount(r, Seq(iceExtensionAggColName), "iceExtensionKm_3")
  }
  
  //-------------------------------------------------------------------------
  private def aggregateAndCountIceExtension(fileList: List[String]) {
  
    val psdTotalColName = "psdTotal"    
    val idColName = "lp_id"
    val iceExtensionColName = "extension[Km3]"
    val iceExtensionAggColName = "extension_Km3"    
    
    
    fileList.map{ f=>
      val station = MyUtil.getParentPath(f).split("/").last
      val dfRaw = MyDataframe.loadCSV(f).get
      val df = dfRaw.select(idColName, psdTotalColName, iceExtensionColName)
        .filter(dfRaw(iceExtensionColName) > 0)

      aggregateAndCountIceExtensionRaw(df.                                        withColumn(iceExtensionAggColName, round(dfRaw(iceExtensionColName))), station + " ALL") 
      aggregateAndCountIceExtensionRaw(df.where(df(idColName) =!=  "NO_LP").      withColumn(iceExtensionAggColName, round(dfRaw(iceExtensionColName))), station + " != NO_LP")        
      aggregateAndCountIceExtensionRaw(df.where(df(idColName) ===  "LP_HIGH_FEQ").withColumn(iceExtensionAggColName, round(dfRaw(iceExtensionColName))), station + " ==LP_HIGH_FEQ")
      aggregateAndCountIceExtensionRaw(df.where(df(idColName) ===  "LP_LOW_FEQ"). withColumn(iceExtensionAggColName, round(dfRaw(iceExtensionColName))), station + " ==LP_LOW_FEQ")
      aggregateAndCountIceExtensionRaw(df.where(df(idColName) ===  "NO_LP").      withColumn(iceExtensionAggColName, round(dfRaw(iceExtensionColName))), station + " ==NO_LP")        
    }
  }
  //-------------------------------------------------------------------------
  private def aggregateAndCountSeasonRaw(df: DataFrame, message: String)  {

    val seasonColName = "season"    
    log.info(message)
    sp.myStat.aggregateAndCount(df, Seq(seasonColName), "lp_count")
  }

  //-------------------------------------------------------------------------
  private def aggregateAndCountSeason(fileList: List[String])  {

    val psdTotalColName = "psdTotal"
    val seasonColName = "season"
    val idColName = "lp_id"    
    
     fileList.map{ f=>
      val station = MyUtil.getParentPath(f).split("/").last
      val df = MyDataframe.loadCSV(f).get.select(idColName, psdTotalColName, seasonColName)
      
      aggregateAndCountSeasonRaw(df                                         ,  station + " ALL") 
      aggregateAndCountSeasonRaw(df.where(df(idColName) =!=  "NO_LP")       ,  station + " != NO_LP")        
      aggregateAndCountSeasonRaw(df.where(df(idColName) ===  "LP_HIGH_FEQ") ,  station + " ==LP_HIGH_FEQ")
      aggregateAndCountSeasonRaw(df.where(df(idColName) ===  "LP_LOW_FEQ")  ,  station + " ==LP_LOW_FEQ")
      aggregateAndCountSeasonRaw(df.where(df(idColName) ===  "NO_LP")       ,  station + " ==NO_LP")        
    }
  }
  
  //-------------------------------------------------------------------------  
  def translateAction(action: Int) {
    if ((action & 1) > 0) log.info("Action 1 => Translate signals from miniseed")
    if ((action & 2) > 0) log.info("Action 2 => Build the surround signals")
    if ((action & 4) > 0) log.info("Action 4 => Detect signals")       
    if ((action & 8) > 0) log.info("Action 8 => Filter and assign id to signals")
    if ((action & 16) > 0) log.info("Action 16 => Save as CSV file seismic data range")
    if ((action & 32) > 0) log.info("Action 32 => Save as CSV file surround data range")
    if ((action & 64) > 0) log.info("Action 64 => Save as CSV file lp data")
    if ((action & 128) > 0) log.info("Action 128 => Save as CSV file psd data")
  } 
 
  //-------------------------------------------------------------------------
  private def buildSessionName {
    
    import java.text.SimpleDateFormat
    val formatter = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");    
    val newDate= formatter.format(new Date())

    mongoUserRunInfo = newDate+"_"+
                       station + "_"+
                       year + "_"+    
                       signalMinTimeMs+"-"+signalMaxTimeMs+"ms_"+
                       timeSliceRangeInS+"Sslice_"+
                       surMinAmplitude+"min_"+
                       surMinTimeToGlueMs+"glue_"+
                       bandPassFilterLowHz+"-"+bandPassFilterHighHz+"Hz_"+
                       lowPassFilterHz+"Hz_"  
  }
 
 //-------------------------------------------------------------------------  
 def process: Boolean = {
   
   val action =                         userArgumentList( 0 ).trim.toInt   
   mongoDatabaseName =                  userArgumentList( 1 ).trim
   station =                            userArgumentList( 2 ).trim
   year =                               userArgumentList( 3 ).trim
   
   val inputMiniSeedRoot =              MyUtil.ensureEndWithFileSeparator(userArgumentList( 4 ).trim)
   val outputRoot=                      MyUtil.ensureEndWithFileSeparator(userArgumentList( 5 ).trim)
   
   signalMinTimeMs =                    userArgumentList( 6 ).trim.toLong    //1500
   signalMaxTimeMs =                    userArgumentList( 7 ).trim.toLong    //40000
   
   timeSliceRangeInS =                  userArgumentList( 8 ).trim.toInt     //3600 Used in surround calculations to measure the noise   
   surMinAmplitude =                    userArgumentList( 9 ).trim.toLong  //2000    
   surMinTimeToGlueMs=                  userArgumentList( 10 ).trim.toLong   //250
     
   bandPassFilterLowHz =                userArgumentList( 11 ).trim.toDouble  //0.5
   bandPassFilterHighHz  =              userArgumentList( 12 ).trim.toDouble  //8
   lowPassFilterHz  =                   userArgumentList( 13 ).trim.toDouble  //1
      
   val dataQueryCSV =                   userArgumentList( 14 ).trim          //data query CSV file
   val dataQueryStart =                 userArgumentList( 15 ).trim.toLong   //start date of the query 
   val dataQueryEnd =                   userArgumentList( 16 ).trim.toLong   //end date of the query
      
   translateAction(action)
   log.info( s"mongoDatabaseName:                                 '$mongoDatabaseName'" )
   log.info( s"station:                                           '$station'" )
   log.info( s"year:                                              '$year'" )
   
   buildSessionName
   val outputPsdCSV = outputRoot + mongoUserRunInfo + "psd.csv"
    
   log.info( s"inputMiniSeedRoot:                                 '$inputMiniSeedRoot'" )
   log.info( s"outputRoot:                                        '$outputRoot'" )
   log.info( s"Output psd csv file  :                             '$outputPsdCSV'" )
   
   log.info( s"Signal min time in ms:                             '$signalMinTimeMs" )
   log.info( s"Signal max time in ms:                             '$signalMaxTimeMs'" )
   
   log.info( s"Time slice range in s:                             '$timeSliceRangeInS'")   
   log.info( s"Min surround amplitude:                            '$surMinAmplitude'" )
   log.info( s"Min time to glue consecutive surround signal in ms:'$surMinTimeToGlueMs")
   
   log.info( s"Filter 1: band pass low filter in Hz:              '$bandPassFilterLowHz'" )  
   log.info( s"Filter 1: Band pass high filter in Hz:             '$bandPassFilterHighHz'" ) 
   log.info( s"Filter 2: low pass filter in Hz:                   '$lowPassFilterHz'" )  
   
   log.info( s"Data query output CSV file:                        '$dataQueryCSV'" )
   log.info( s"Start date of the query in ms:                     '$dataQueryStart'" )
   log.info( s"End date of the query in ms:                       '$dataQueryEnd'" )
    
   //mongoDB   
   setMongoCollection
    
   //query seismic build
   seismic = new QuerySeismic(sp, sp.getSparkSession, sp.getSparkSQLContext, seismicMDB)
      
   if ((action & 1) > 0)
     translateMiniSeedData(inputMiniSeedRoot)
         
   //build the surround signal
   if ((action & 2) > 0)
     seismic.buildSurroundSignal(timeSliceRangeInS    
       , bandPassFilterLowHz 
       , bandPassFilterHighHz
       , lowPassFilterHz 
       , surMinAmplitude) 
       
    //detect the lp  
    if ((action & 4) > 0)       
      seismic.detectSignal(signalMinTimeMs,  signalMaxTimeMs, surMinTimeToGlueMs)
        
    //filter LP and assign ID according to power density spectrum
    if ((action & 8) > 0)
      seismic.assignID_toSignal(outputPsdCSV)
      
    //Save CSV seismic data
    if ((action & 16) > 0)  seismic.saveCSV_SeismicData(dataQueryCSV,dataQueryStart,dataQueryEnd)
    
    //Save CSV surround data
    if ((action & 32) > 0)  seismic.saveCSV_SurroundData(dataQueryCSV,dataQueryStart,dataQueryEnd)
    
    //Save CSV lp data
    if ((action & 64) > 0)  seismic.saveCSV_LP_Data(dataQueryCSV,dataQueryStart,dataQueryEnd)
    
    //Save CSV psd data
    if ((action & 128) > 0)  seismic.saveCSV_PSD_Data(dataQueryCSV,dataQueryStart,dataQueryEnd)
      
    //close Mongo connections    
    seismic.closeMongoConnections
    closeMongoCollection  
                                                
    true
  }
 
  //-------------------------------------------------------------------------
  override def main {
    
    //main code    
    loadLastUserSettingList                       //load last user query
    Query.initSeasonByYear                        //init the whether seasons in all parsed years  
  
    log.info(s"Program version: $MajorVersion.$MinorVersion.$Compilation")
    log.info(s"User: '$userName'")
    log.info("Running in a computer with operating system: '"+MyUtil.OsName+"'")
    log.info( s"The data are filtered by a pass band filter, and the result signal is transformed into a surround value " +
                "applying a low pass buttweworth filter");
          
    //range date
    parseDateRange

    //main work
    process
    
    
    //processIceExtension
  //  processMeteorology  //take care and use only the data of the proper station: ccv, dcp or lvn
    //processTide
     
   // joinData
    //kmeans
   // kNearestNeighbor
      
    //joinData
    
    //aggregateAndCountSignal
    //aggregateAndCountSeason
    
    //aggregateAndCountIceExtension
   // stats
      
     //SandBox.test   
  }
  
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
} //End of case class 'IceSpark'

//=============================================================================
// End of 'iceSpark.scala' file
//=============================================================================
