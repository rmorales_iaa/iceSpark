//=============================================================================
//File: main.scala
//=============================================================================
/** It is the main entrance of Boom!. See object declaration
 *  @author  Rafael Morales Muñoz
 *  @mail    rmorales.iaa.es
 *  @version 1.0
 *  @date    9 Nov 2016
 *  @history None
 */
//=============================================================================

package iceSpark

//=============================================================================
// System import section
//=============================================================================
import org.apache.spark.sql.{DataFrame, SparkSession}

//=============================================================================
// User import section
//=============================================================================
import catSpark.configuration.MyConf
import catSpark.gui.GuiUtil
import catSpark.logger.visual.LoggerVisual
import catSpark.util.MyUtil
import catSpark.spark.dataFrame.MyDataframe
import catSpark.spark.MySpark

//=============================================================================
// Class/Object implementation
//=============================================================================
import catSpark.spark.dataFrame.MyDataframe


//=============================================================================
// Class/Object implementation
//=============================================================================
//=============================================================================
object SandBox {
   
  //-------------------------------------------------------------------------
  
  var log : LoggerVisual = null
  var sp : MySpark = null
  var sparkSession: SparkSession = null
  var main : IceSpark = null
  
  //-------------------------------------------------------------------------  
  def filterByTimeRange( s:String     
    , dateStart: Long
    , dateEnd: Long 
    , out: String ){
    
    val df = MyDataframe.loadParquetFile( s )
    val colName =  iceSpark.query.Query.schemaTimeStampCol 
    val newDF = df.filter( df( colName ) >= dateStart )
                  .filter( df( colName ) <= dateEnd )
                  .sort ( colName )
    
    MyDataframe.saveDataframeAsParquet(newDF, out)
    
    newDF.printSchema()    
    newDF.show()
  }

  //-------------------------------------------------------------------------  
  def test_df ( s: String, sortColName: String ) {
   
    log.info(s"============ Parsing: '$s' ============")

    val df = MyDataframe.loadParquetFile( s )

    df.printSchema()
    log.info( "Total samples= "+ df.count )
    val sortedAsc = df.sort( sortColName )
    log.info("-------- Top 5 Min-Max below -------- ")
    sortedAsc.take(5).foreach( row => log.info( row.toString ) )
    
    import org.apache.spark.sql.functions._
    val sortedDesc = df.sort(desc( sortColName ))
    SandBox.log.info ("++++++++ Top 10 Max-Min below ++++++++")
    sortedDesc.take(10).foreach( row => SandBox.log.info( row.toString ))
    
    SandBox.log.info (s"============ End of parsing: '$s' ============ ")
  }
  
  //-------------------------------------------------------------------------  
  private def write_result ( r: List[Double] , s: String)
  {
    import java.io.{File,FileWriter,BufferedWriter,FileOutputStream, OutputStreamWriter}    
    val writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(s)))
    for (z <- r) {
        writer.write(z + "\n")  // however you want to format it
    }
    writer.close()           
  }
  
  //-------------------------------------------------------------------------
  def haddopTest{
        
    
    import catSpark.hadoop.MyHadoop
    
    if (MyHadoop.directoryExist("hdfs:///spark")) println("EXISTS DIR!!!")
    if (MyHadoop.fileExist("hdfs:///spark/spark-libs.jar")) println("EXISTS!!!")
    
    MyHadoop.createDirectoryIfNotExist("hdfs:///kk")
    MyHadoop.delete("hdfs:///kk")
  }
  
  //-------------------------------------------------------------------------

  def fftSpeedTest_1{
    
    import scala.io.Source
    val l = Source.fromFile("/home/router/Downloads/rand10K.txt").getLines.map(_.toDouble).toList
    
    val startTime = System.currentTimeMillis
    val fftScalaSignal = catSpark.signal.FFT.fft(l)     //use scala signal for fft
    log.info("Elapsed time scala:" + MyUtil.getString(System.currentTimeMillis-startTime))
      
  }

  //-------------------------------------------------------------------------

  def fftSpeedTest_2 {
    
    import scala.io.Source
    val l = Source.fromFile("/home/router/Downloads/rand10K.txt").getLines.map(_.toFloat).toArray
    
    val startTime = System.currentTimeMillis
    val fftScalaSignal = iceSpark.wrapper.fftsNative.calculate(l)     //use scala signal for fft
    log.info("Elapsed time native:" + MyUtil.getString(System.currentTimeMillis-startTime))
      
  }
  
  //-------------------------------------------------------------------------
  
  import catSpark.hadoop.MyHadoop
  
  def haddopCreateFile {
  
   //create the output file
   val outputSurCSV = "myFile"
   MyHadoop.delete(outputSurCSV)
   val file = MyHadoop.createOutputFileIfNotExist(outputSurCSV)   
   MyHadoop.writeInto("alteta", file)
   MyHadoop.close(file)
}

  //-------------------------------------------------------------------------
  
  private def fft_test(){
   
  //based on https://gist.github.com/awekuit/7496127
  import breeze.linalg.DenseVector
  import breeze.math._
  import edu.emory.mathcs.jtransforms.fft.{DoubleFFT_1D => jFFT}

  def show(v0: Array[_], v1 : Array[_], v2 : Array[_], v3 : Array[_]) {
    println("scala signal : " + v0.mkString(", "))
    println("breeze       : " + v1.mkString(", "))
    println("jt zero dummy: " + v2.mkString(", "))
    println("jt           : " + v3.mkString(", "))
    println
  }
  
  val data0 = Array[Double](3, 1, 4, 1, 5, 9, 2, 6)
  val data1 = data0.clone
  val data2 = data0.clone
  val data3 = data0.clone
    
  show(data0, data1, data2, data3)
    
  val fftScalaSignal = catSpark.signal.FFT.fft(data1.toList)     //use scala signal for fft
    
  val fftBreezeFFT = breeze.signal.fourierTr(DenseVector(data1)) // use Breeze for fft    
    
  val fftJT_ZeroDummy = {                                       // use jFFT
    val temp = data2 ++ Array.fill(data2.size)(0.0)     // add dummy      
    val fft = new jFFT(data2.size)                      // attention!! data2.size == temp.size / 2
    fft.realForwardFull(temp)                         // Side Effect!
    temp
  }
    
  val fftJT = {
    val fft = new jFFT(data3.size)
    fft.realForward(data3)
    val temp = data3(1)
    val tail = data3.drop(2)
     Array(data3.head, 0) ++ tail ++ Array(temp, 0) ++ tail.reverse
  }
  show(fftScalaSignal.toArray, fftBreezeFFT.activeIterator.map(_._2).toArray, fftJT_ZeroDummy, fftJT)
    
  // Power Spectral
  val minPower = 1e-20 * data1.size * data1.size
  val toPower : (Double, Double) => Double = (re, im) => Math.log10((re * re + im * im) max minPower)
    
  val powerScalaSignal = fftScalaSignal.map{ case (Complex(re, im)) =>  toPower(re, im)}.toArray    
  val powerBreeze = fftBreezeFFT.activeIterator.map{ case (_, Complex(re, im)) =>  toPower(re, im)}.toArray       
  val powerJT_ZeroDummy = fftJT_ZeroDummy.grouped(2).map{case Array(re, im) => toPower(re, im)}.toArray
  val powerJT = fftJT.grouped(2).map{case Array(re, im) => toPower(re, im)}.toArray
    
  show(powerScalaSignal, powerBreeze, powerJT_ZeroDummy, powerJT)

  // IFFT
    
  val inverseScalaSignal  =  catSpark.signal.FFT.ifft(fftScalaSignal).map{ case (Complex(re, im)) => re}
    
  val inverseBreeze = breeze.signal.iFourierTr(DenseVector(powerBreeze)).activeIterator.map(_._2.real).toArray
       
  val inverseFull = (powerSpectral : Array[Double]) => {
    val temp = powerSpectral ++ Array.fill(powerSpectral.size)(0.0) // add dummy
    val fft = new jFFT(powerSpectral.size)
    fft.realInverseFull(temp, true)
    temp.grouped(2).map(_.head).toArray
  }
    
  val inverseJT_ZeroDummy = inverseFull(powerJT_ZeroDummy)
  val inverseJT = inverseFull(powerJT) 
    
  // show spectrum
   show(inverseScalaSignal.toArray, inverseBreeze, inverseJT_ZeroDummy, inverseJT)
}
  
//-------------------------------------------------------------------------
  
def test {
   
    //translateMiniSeed

    /*
    filterByTimeRange ( "/media/data2TB/vane/parquet/seismic/deception_isle/2009/z/"
        
       ,  1251763200000L //2009-09-2009 00:00:00
       ,  1254355200000L //2009-10-2009 00:00:00       
      //, 1204374375000L //"2008-03-01 12:26:15"
      //, 1204374378000L// "2008-03-01 12:26:30"
       
      , "/media/data2TB/vane/lp_test/parquet")
      */
      
    test_df ("/media/data2TB/vane/lp_test/parquet",  iceSpark.query.Query.schemaTimeStampCol )
    //test_query
    
    //testUDF
  }

  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
} //end of object 'SandBox'

//=============================================================================
// End of 'SandBox.scala' file
//=============================================================================
