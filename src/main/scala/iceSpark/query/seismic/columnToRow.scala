//=============================================================================
//File: columnToRow.scala
//=============================================================================
/** It implements aggregates all column value to one single row
//https://stackoverflow.com/questions/32100973/how-to-define-and-use-a-user-defined-aggregate-function-in-spark-sql
 *  @author  Rafael Morales Muñoz
 *  @mail    rmorales.iaa.es
 *  @version 1.0
 *  @date    28 Sept 2017
 *  @history None
 */
//=============================================================================
//=============================================================================
// Package section
//=============================================================================
package iceSpark.query.seismic

//=============================================================================
// System import section
//=============================================================================
import org.apache.spark.sql.Row
import org.apache.spark.sql.expressions.{MutableAggregationBuffer, UserDefinedAggregateFunction}
import org.apache.spark.sql.types._

//=============================================================================
// User import section
//=============================================================================

//=============================================================================
// Class/Object implementation
//=============================================================================

//Extend UserDefinedAggregateFunction to write custom aggregate function

class ColumnToRow() extends UserDefinedAggregateFunction {

  // Data Type Schema of the input data
  def inputSchema: StructType = StructType(Array(StructField("item", IntegerType)))
 
  // Intermediate Schema
  def bufferSchema = StructType(Array(
    StructField("result", StringType)
  ))
 
  // Returned Data Type .
  def dataType: DataType = StringType
 
  // Self-explaining
  def deterministic = true
 
  // This function is called whenever key changes
  def initialize( scheme: MutableAggregationBuffer) = {
    scheme(0) = "" // set sum to zero    
  }
 
  // Iterate over each entry of a group
  def update( scheme: MutableAggregationBuffer, row: Row) = {
    scheme(0) += row(0).toString
  }
 
  // Merge two partial aggregates
  def merge( scheme: MutableAggregationBuffer, row: Row) = {     
    scheme(0) += row(0).toString()
  }
 
  // Called after all the entries are exhausted.
  def evaluate(buffer: Row) = {
    buffer.getString(0) 
  }
 
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
} //end of case class 'ColumnToRow'
//=============================================================================
// End of 'columnToRow.scala' file
//=============================================================================
