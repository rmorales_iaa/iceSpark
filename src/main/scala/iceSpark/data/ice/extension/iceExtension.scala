//=============================================================================
//File: tide.scala
//=============================================================================
/** It implements a data manager for ice extension data
 *  @author  Rafael Morales Muñoz
 *  @mail    rmorales.iaa.es
 *  @version 1.0
 *  @date    24 Aug 2016
 *  @history None
 */
//=============================================================================
//=============================================================================
// Package section
//=============================================================================
package iceSpark.data.ice.extension

//=============================================================================
// System import section
//=============================================================================
import java.util.Date
import java.io.File

import org.apache.spark.sql.{DataFrame, Row, SparkSession}
import org.apache.spark.sql.types._

import org.joda.time.{ DateTime, DateTimeZone }
import org.joda.time.format.DateTimeFormat

//=============================================================================
// User import section
//=============================================================================
import catSpark.configuration.MyConf
import catSpark.database.mongoDB.MyMongoDB
import catSpark.logger.visual.LoggerVisual
import catSpark.spark.dataFrame.MyDataframe
import catSpark.util.MyUtil

//=============================================================================
// Class/Object implementation
//=============================================================================
object IceExtension {
  //-------------------------------------------------------------------------
  //Class variables section
  //-------------------------------------------------------------------------
  var log : LoggerVisual = null
  var sparkSession: SparkSession = null

  //-------------------------------------------------------------------------  
  //configuration  
  private val InputPath =  MyUtil.ensureEndWithFileSeparator(MyConf.getString( "IceSpark.data.ice.extension.rootInputPath" ))
  private val OutputPath = MyUtil.ensureEndWithFileSeparator(MyConf.getString( "IceSpark.data.ice.extension.rootoOutPath" ))

  //-------------------------------------------------------------------------
  //dataframe
  private val DataTimeFormat = DateTimeFormat.forPattern("yyyy:MM:dd").withZone(DateTimeZone.UTC)
  
  private val schema = new StructType()
    .add(StructField("timeStamp[ms]" ,        LongType,  false)) 
    .add(StructField("extension[Km3]" ,       FloatType, false))
    .add(StructField("extensionMissing[Km3]", FloatType, false))
    
  val nameDataList = "_id" :: (schema.fields.map { f => f.name }.drop(1).toList)  
  
  //-------------------------------------------------------------------------
  //time range
  private var startDate: Long = 0
  private var endDate: Long = 0
    
  //-------------------------------------------------------------------------  
  //file extension  
  private val FileExtensionData = ".csv"
  private val ValidFileExtension = List (FileExtensionData)  
  
  //-------------------------------------------------------------------------  
  //mongo connection
  private var mongoConnection: MyMongoDB = null
  
  //-------------------------------------------------------------------------
  // Code starts
  //-------------------------------------------------------------------------
  
  //-------------------------------------------------------------------------
  private def createDataFrame(l : List[Option[ (Long, Float, Float) ]]) : DataFrame = {
      
    val rdd = sparkSession.sparkContext.parallelize(l).map { r => Row( (r.get._1), (r.get._2), (r.get._3)) }    
    sparkSession.createDataFrame( rdd, schema )  
  }
  
  //-------------------------------------------------------------------------
  private def translateFile( f: File ) {
    import scala.io.Source
   
    val startTime = System.currentTimeMillis
    val fFull = f.getAbsolutePath
    val fName = MyUtil.getPathOnlyFilename(fFull)
    val parent = MyUtil.getParentPath(fFull)
    
    log.info(s"Processing file '$fName'")
    
    //parse all lines
    val l = Source.fromFile(fFull).getLines().toList.map { line =>
      
      if ((line.startsWith("Year")) || (line.startsWith("YYYY"))) None
      else {
        val split = line.split("\\s+").mkString.split(",")
        if (split.size == 6){
          val date = DataTimeFormat.parseDateTime( split.take( 3 ).mkString( ":" ) ).getMillis         
          val extensionKm3 =        split.takeRight(3)(0).toFloat
          val extensionMissingKm3 = split.takeRight(2)(0).toFloat
          if  ( (date >= startDate) && (date <= endDate) ) Some( date , extensionKm3, extensionMissingKm3 )
          else None  
        }
        else None            
      }       
    }.filter(_.isDefined)

    val df = createDataFrame ( l )
    mongoConnection.saveDF_Overwrite(df.toDF(nameDataList.toSeq:_*))
    
    //this creates the parquet file
    //MyDataframe.saveDataframeAsParquet( df, OutputPath+parent, fName )
        
    log.info("Elapsed time:" + MyUtil.getString(System.currentTimeMillis-startTime))
  }  
  
  //-------------------------------------------------------------------------
  private def translateDirectory( baseDir:  String ) {

    MyUtil.getSortFileList(baseDir,ValidFileExtension).foreach { translateFile( _ ) }      
     
    MyUtil.getSortSubDirectoryList( baseDir ).foreach{ d => 
      MyUtil.getSortFileList(d.getAbsolutePath,ValidFileExtension).foreach { translateFile( _ ) }      
      translateDirectory(d.getAbsolutePath) //recursive call
    }    
  }
  
  //-------------------------------------------------------------------------
  def translateDirectory(_startDate: DateTime, _endDate: DateTime, _mongoConnection: MyMongoDB){
    
    //set the range date  
    startDate = _startDate.getMillis
    endDate = _endDate.getMillis
    
    //set mongoConnection
    mongoConnection = _mongoConnection
    
    translateDirectory(InputPath)
  }
  
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
} //end of object 'IceExtension'
//=============================================================================
// End of 'iceExtension.scala' file
//=============================================================================
