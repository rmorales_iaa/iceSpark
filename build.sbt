//=============================================================================
//General comment
//=============================================================================
//After editing this file run "sbt eclipse" where *.sbt file is stored
//to generate a project compatible with eclipse

//=============================================================================

//add extra settings to be added to BuildInfoKey (see below)
// to be available in scala source
lazy val authorList =         SettingKey[String]("authorList")
lazy val license =            SettingKey[String]("license")
lazy val myGitCurrentBranch = SettingKey[String]("myGitCurrentBranch")
lazy val myGitHeadCommit =    SettingKey[String]("myGitHeadCommit")

//common settings
//version will be calculated using git repository
lazy val commonSettings = Seq(
    name :=                 "iceSpark"
    , description :=        "Tool based on Spark to process ice data"
    , organization :=       "IAA-CSIC"
    , authorList  :=        "Rafael Morales (rmorales@iaa.es)"
    , homepage :=           Some(url("http://www.iaa.csic.es"))
    , license :=            "This project is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)"
    , myGitCurrentBranch := git.gitCurrentBranch.value
    //, myGitHeadCommit :=    git.gitHeadCommit.value.get
)

//Main class
lazy val mainClassName = "iceSpark.Main"

//fix scalaVersion 
scalaVersion := "2.11.11"

//fix version. This avoid to generate an jar with a different name is a tag is not set in the git
version := "1.0.0-SNAPSHOT"

//=============================================================================
//used versions

//spark
val sparkVersion = "2.2.0"

//akka
val akkaVersion = "2.5.6"

//=============================================================================
//library dependences

resolvers ++= Seq(
  "Hadoop Releases" at "https://repository.cloudera.com/content/repositories/releases/"
)

//other dependece list
lazy val dependenceList = Seq(
    
    //manage configuration files. https://github.com/typesafehub/config
    "com.typesafe" % "config" % "1.3.2"
    
    //git management. https://github.com/centic9/jgit-cookbook
    ,  "org.eclipse.jgit" % "org.eclipse.jgit" % "4.9.0.201710071750-r"
    
   //log management
    ,  "ch.qos.logback" % "logback-classic" % "1.2.3"
    ,  "org.apache.logging.log4j" % "log4j-core" % "2.9.1"
    ,  "org.slf4j" % "slf4j-log4j12" % "1.7.25"
    
    //GUI. https://mvnrepository.com/artifact/org.scala-lang/scala-swing/2.11.0-M7
    ,  "org.scala-lang.modules" % "scala-swing_2.11" % "2.0.0"
    
    //CSV (command separated value) library    
    , "org.apache.commons" % "commons-csv" % "1.5"
    
    //time management
    ,  "joda-time" % "joda-time" % "2.9.9"
    ,  "org.joda" % "joda-convert" % "1.8.2"
    
    //actors and akka (thread paralelization)
    // https://mvnrepository.com/artifact/com.typesafe.akka/akka-actor_2.11
    ,  "com.typesafe.akka" % "akka-actor_2.11"  % akkaVersion
    ,  "com.typesafe.akka" % "akka-slf4j_2.11"  % akkaVersion exclude("org.slf4j", "slf4j-api")
      
    //apache commons
    ,  "commons-io" % "commons-io" % "2.5"
    ,  "org.apache.commons" % "commons-lang3" % "3.6"
        
    //spark
    ,  "org.apache.spark" % "spark-mllib_2.11"  % sparkVersion
    ,  "org.apache.spark" % "spark-graphx_2.11" % sparkVersion
    
    //hadoop. Distributed file system (hdfs) and resource manager (yarn)   	
    ,  "org.apache.hadoop" % "hadoop-common" % "2.8.1"
    ,  "org.apache.hadoop" % "hadoop-mapred" % "0.22.0"
    ,  "org.apache.hadoop" % "hadoop-hdfs" % "2.8.1"
    
    //Convert between java and scala collections. https://github.com/scalaj/scalaj-collection
    ,  "org.scalaj" % "scalaj-collection_2.11" % "1.6"
    
    //SPOIWO (Scala POI Wrapping Objects).  https://github.com/norbert-radyk/spoiwo
    // POI: Java API for Microsoft Documents
    ,  "com.norbitltd" % "spoiwo_2.11" % "1.2.0"
    
    //manage compression formats and file utils. https://commons.apache.org/proper/commons-compress/
    ,  "org.apache.commons" % "commons-compress" % "1.14"
    
    //time series management. https://github.com/sryza/spark-timeseries
    ,  "com.cloudera.sparkts" % "sparkts" % "0.4.1"
    
    //linear algebra for signal processing. http://www.scalanlp.org/
    ,  "org.scalanlp" % "breeze_2.11" % "0.13.2"
      
     // Multithreaded FFT (fast fourier transformation) library written in pure Java. http://sourceforge.net/projects/jtransforms/   
    ,  "net.sourceforge.jtransforms" % "jtransforms" % "2.4.0"
    
    
    , "org.apache.commons" % "commons-math3" % "3.6.1"
    
    //mongoDB. Document database: https://github.com/mongodb/casbah  
    ,  "org.mongodb" % "casbah-core_2.11" % "3.1.1"
    ,  "org.mongodb" % "casbah-commons_2.11" % "3.1.1"
    ,  "com.stratio.datasource" %% "spark-mongodb" % "0.12.0"    
)

//=============================================================================
//excluding and overrides

//remove the library slf4j-log4j12-1.7.10.jar to avoid multiple SLF4J bindings
//the only SLF4J binding will be at logback-classic

libraryDependencies := libraryDependencies.value.map(_.exclude("org.slf4j", "slf4j-log4j12"))

//=============================================================================
//resolvers

resolvers ++= Seq(
    Resolver.sonatypeRepo("public")
    , "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"
)

//=============================================================================
//build info options. check the generated scala file: BuildInfo.scala
buildInfoOptions += BuildInfoOption.BuildTime
buildInfoKeys += buildInfoBuildNumber


//=============================================================================

//merge strategy
assemblyMergeStrategy in assembly  := {
    
    case PathList("META-INF", xs @ _*) => MergeStrategy.discard
    case "about.html" => MergeStrategy.rename    
    case _ => MergeStrategy.last
}

//=============================================================================
//root project
lazy val root = (project in file("."))
    .enablePlugins(BuildInfoPlugin
                   ,GitVersioning)
    .settings(commonSettings: _*)
    .settings(libraryDependencies ++= dependenceList)
    .settings(mainClass in (assembly) := Some(mainClassName))    
    .settings(buildInfoKeys := Seq[BuildInfoKey](name
                                                , version
                                                , description
                                                , organization
                                                , authorList
                                                , license
                                                , scalaVersion
                                                , sbtVersion
                                                , buildInfoBuildNumber
                                                , myGitCurrentBranch
                                                , myGitHeadCommit)
              , buildInfoPackage := "BuildInfo")   //Name of the file and package. 
                                                   //File generated at 
                                                   //'target/scala-2.11/src_managed/main/sbt-buildinfo/BuildInfo.scala'

//=============================================================================
//=============================================================================
//End of file
//=============================================================================
