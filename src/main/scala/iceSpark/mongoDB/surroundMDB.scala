//=============================================================================
//File: surroundMDB.scala
//=============================================================================
/** It implements a MongoDB client for seismic surround data 
 *  @author  Rafael Morales Muñoz
 *  @mail    rmorales.iaa.es
 *  @version 1.0
 *  @date    10 Dec 2017
 *  @history None
 */
//=============================================================================
//=============================================================================
// Package section
//=============================================================================
package iceSpark.mongoDB

//=============================================================================
// System import section
//=============================================================================
import com.mongodb.casbah.{ MongoCollection }
import com.mongodb.casbah.Imports._

import org.apache.spark.sql.types._
import org.apache.spark.sql.{ DataFrame, Row, SQLContext, SparkSession }
  
//=============================================================================
// User import section
//=============================================================================
import catSpark.logger.visual.LoggerVisual
import catSpark.database.mongoDB.MyMongoDB
import catSpark.spark.dataFrame.MyDataframe
import catSpark.util.MyUtil

//=============================================================================
// Class/Object implementation
//=============================================================================
import MyMongoDB._

class SurroundMDB( dbName: String, collectionName: String ) extends MyMongoDB(dbName, collectionName) {
  
  //-------------------------------------------------------------------------
  // Constants
  //-------------------------------------------------------------------------
    
   private final val MONGO_ID= "_id"    //long
  
   val schemaTimeStampCol=    "timeStamp"  
   val schemaIdCol    =       "id"  
   val schemaBandPassCol =    "passBand_value"
   val schemaSurroundCol =    "surround_value"  
  
   val dfSchemaWithMongoID = new StructType()
      .add( StructField( MONGO_ID,            LongType,   false))
      .add( StructField( schemaSurroundCol,   DoubleType, false))
      .add( StructField( schemaBandPassCol,   DoubleType, false))
      
   val dfSchema = new StructType()
      .add( StructField( schemaTimeStampCol,  LongType,   false))
      .add( StructField( schemaSurroundCol,   DoubleType, false))
      .add( StructField( schemaBandPassCol,   DoubleType, false))
                                          
  //-------------------------------------------------------------------------
  // Code starts
  //-------------------------------------------------------------------------

   connect

  //-------------------------------------------------------------------------
  def renameFieldList(df: DataFrame) = df.toDF( Seq(schemaTimeStampCol, schemaSurroundCol, schemaBandPassCol): _*)
  
  //-------------------------------------------------------------------------  
  def renameFieldListWithMongoID(df: DataFrame) = df.toDF( Seq(MONGO_ID, schemaSurroundCol, schemaBandPassCol): _*)   
  
  //-------------------------------------------------------------------------
  def loadDF = renameFieldList(super.loadDF( dfSchemaWithMongoID ))      

  //-------------------------------------------------------------------------
  def getSurroundSignal( start: Long, end: Long ) = {
    
     val q = new MongoDBObject(MONGO_ID  $gte start  $lte end )
     
     //projection
     val p = MongoDBObject( schemaIdCol -> 0 ) ++= MongoDBObject( schemaSurroundCol -> 0 )  
         
     //sort
     val s =  MongoDBObject( MONGO_ID -> 1) 
       
     //execute the query+projection+sort
     collection.find( q, p )
      .sort( s )
      .map( obj => obj.get( schemaSurroundCol ).asInstanceOf[Double] ).toList      
  }
  
   //-------------------------------------------------------------------------
  def query(start: Long, end: Long) = {
    
     val q = new MongoDBObject(MONGO_ID  $gte start  $lte end )
         
     //sort
     val s =  MongoDBObject( MONGO_ID -> 1) 
       
     //execute the query+sort
     collection.find( q ).sort( s )
  }

  //-------------------------------------------------------------------------
  private def getList( r: MongoCursor ) = {
    
    r.map { obj => 
        val t = obj.get( MONGO_ID ).asInstanceOf[Long]
        val s = obj.get( schemaSurroundCol ).asInstanceOf[Double]
        val p = obj.get( schemaBandPassCol ).asInstanceOf[Double]
        (t,s,p)
      }.toList         
  }
  
  //-------------------------------------------------------------------------
  def getDataFrame(start: Long, end: Long) = getDadataFrame(query(start,end))
  
  //-------------------------------------------------------------------------  
  private def getDadataFrame( r: MongoCursor ) = {
    
    val l = getList(r) 
    
    sparkSession.createDataFrame(sparkSession.sparkContext.parallelize( l ).map { w => Row( w._1, w._2, w._3 ) }    
      , dfSchema )       
  }
  
  //-------------------------------------------------------------------------  
  def save(data: List[(Long,Double,Double)]){
        
    try {                       
       val builder = collection.initializeUnorderedBulkOperation        
       
       data map ( t=> builder.insert( 
           MongoDBObject(MONGO_ID -> t._1) ++= 
           MongoDBObject(schemaSurroundCol -> t._2) ++=
           MongoDBObject(schemaBandPassCol -> t._3) 
           ))
    
      //insert in batch mode
      val r = builder.execute
      if (r.isAcknowledged) log.info( s"Imported " + r.getInsertedCount + " surround values into mongo" )   
      else log.error(s"Error importing data into mongoDB")
    } 
    catch {
      case e:Exception => log.error(e.toString)
    } 
  }
  
  //-------------------------------------------------------------------------  
  def saveAsCVS(data: List[(Long,Double,Double)],fileName:String){
        
    import java.io.{ BufferedWriter, File, FileWriter }
    val file = new BufferedWriter( new FileWriter( new File( fileName )))
    file.write(List("timeStamp","surroundValue","passBandValue").mkString(",") + MyUtil.LineSeparator)
    data.map(t => {
      file.write(t.productIterator.toList.mkString(",") + MyUtil.LineSeparator)
    })    
             
    file.close
  }
  
  //-------------------------------------------------------------------------
  def getMinMaxRangeID = {
    
     val q = new MongoDBObject("_id"  $gte 0)
     
     //projection
     val p = MongoDBObject( schemaSurroundCol -> 0 ) ++=  MongoDBObject( schemaBandPassCol -> 0 )
         
     //sort
     val sMin =  MongoDBObject( MONGO_ID -> 1)
     val sMax =  MongoDBObject( MONGO_ID -> -1)
            
    //execute the query+projection+sort
    (collection.findOne( q, p, sMin ).get( MONGO_ID ).asInstanceOf[Long] , 
     collection.findOne( q, p, sMax ).get( MONGO_ID ).asInstanceOf[Long])      
  }
  
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
} //end of class 'surroundMDB'
//=============================================================================
// End of 'SurroundMDB.scala' file
//=============================================================================
