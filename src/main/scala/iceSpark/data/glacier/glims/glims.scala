//=============================================================================
//File: glims.scala
//=============================================================================
/** It implements a data manager for glaciar data (glims)
 *  http://glims.colorado.edu/glacierdata/
 *  @author  Rafael Morales Muñoz
 *  @mail    rmorales.iaa.es
 *  @version 1.0
 *  @date    9 Nov 2016
 *  @history None
 */
//=============================================================================
//=============================================================================
// Package section
//=============================================================================
package iceSpark.data.glacier.glims

//=============================================================================
// System import section
//=============================================================================
import java.util.Date
import java.io.File

import org.apache.spark.sql.{DataFrame, Row, SparkSession}
import org.apache.spark.sql.types._

import org.joda.time.{ DateTime, DateTimeZone }
import org.joda.time.format.DateTimeFormat

//=============================================================================
// User import section
//=============================================================================
import catSpark.configuration.MyConf
import catSpark.logger.visual.LoggerVisual
import catSpark.spark.dataFrame.MyDataframe
import catSpark.util.MyUtil

//=============================================================================
// Class/Object implementation
//=============================================================================
object Glims {
  //-------------------------------------------------------------------------
  //Class variables section
  //-------------------------------------------------------------------------
  var log : LoggerVisual = null
  var sparkSession: SparkSession = null

  //-------------------------------------------------------------------------  
  //configuration  
  private val InputPath = MyUtil.ensureEndWithFileSeparator(MyConf.getString( "IceSpark.data.glacier.rootInputPath" ))
  private val OutputPath = MyUtil.ensureEndWithFileSeparator(MyConf.getString( "IceSpark.data.glacier.rootoOutPath" ))
  
  private val glacierList = MyConf.getStringList("IceSpark.data.glacier.glacierListName")

  //-------------------------------------------------------------------------
  //dataframe
  private val ColNameName = "glac_id"
  private val ColNameTimeStamp = "src_date"
  private val ColNameFinaTimeStamp = "timeStamp[ms]"
  
  private val SeletedColList = List( ColNameTimeStamp, ColNameName, "line_type", "area", "width", "length", "min_elev", "max_elev" )

  private val DataTimeFormat = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss").withZone(DateTimeZone.UTC)
  
  //-------------------------------------------------------------------------
  //time range
  private var startDate: java.sql.Date = null
  private var endDate: java.sql.Date = null
    
  //-------------------------------------------------------------------------  
  //file extension  
  private val FileExtensionDBF = ".dbf"
  private val ValidFileExtension = List (FileExtensionDBF)  //observings are grouped in triplets, so take one one
  
  //-------------------------------------------------------------------------
  // Code starts
  //-------------------------------------------------------------------------
  import catSpark.database.dbf._
  def dbfFile(filePath: String) = 
    sparkSession.sqlContext.baseRelationToDataFrame(DBFRelation(filePath)(sparkSession.sqlContext))
  
  //-------------------------------------------------------------------------  
  import org.apache.spark.sql.functions.udf
  
  val timeConversion = udf { (s: String) =>  { DataTimeFormat.parseDateTime(s.trim).getMillis }}

  //-------------------------------------------------------------------------
  private def translateFile( f: File ){
   
    val startTime = System.currentTimeMillis
    val fFull = f.getAbsolutePath
    val fName = MyUtil.getPathOnlyFilename(fFull)
    val parent = MyUtil.getParentPath(fFull)
       
    log.info(s"Processing file '$fName'")               
    val r = MyDataframe.filterByColName( dbfFile(fFull) , SeletedColList )
    if (r._1){
      
      val df = r._2.filter( r._2( ColNameTimeStamp ) >= startDate )    //filter start date
                   .filter( r._2( ColNameTimeStamp ) <= endDate )      //filter end date  
                   .filter( r._2( ColNameName ).isin( glacierList:_* ) ) //filter glaicer name
                   .withColumn( ColNameFinaTimeStamp, timeConversion( r._2( ColNameTimeStamp ))) //translate the time stamp
                   .drop(ColNameTimeStamp)

      val finalDF = MyDataframe.moveLastColumnToFirst(df)
      if (finalDF.count() > 0){      

        //save to parquet
        MyDataframe.saveDataframeAsParquet( df, OutputPath+parent, fName )
        log.info(s"End of processing file '$fName'")
      }
      else log.warning(s"The following file has not data after applying the filters by time and glaciar ID: '$fFull'") 
    }
    else log.error(s" Error parsing file '$fName' " )   
      
    log.info("Elapsed time:" + MyUtil.getString(System.currentTimeMillis-startTime))
  }
  
  //-------------------------------------------------------------------------
  private def translateDirectory( baseDir:  String ) {

    MyUtil.getSortFileList(baseDir,ValidFileExtension).foreach { translateFile( _ ) }      
     
    MyUtil.getSortSubDirectoryList( baseDir ).foreach{ d => 
      MyUtil.getSortFileList(d.getAbsolutePath,ValidFileExtension).foreach { translateFile( _ ) }      
      translateDirectory(d.getAbsolutePath) //recursive call
    }    
  }
  
  //-------------------------------------------------------------------------
  def translateDirectory(_startDate: DateTime, _endDate: DateTime, baseDir:  String = InputPath){
    
    import iceSpark.IceSpark
    
    log.info("Using the following glacier list: " + glacierList.mkString(","))
    
    //set the range date  
    startDate = new java.sql.Date(_startDate.getMillis) 
    endDate = new java.sql.Date(_endDate.getMillis) 
    
    translateDirectory(baseDir)    
  }
  
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
} //end of object 'Glims'
//=============================================================================
// End of 'glims.scala' file
//=============================================================================
