//=============================================================================
//File: seismicMDB.scala
//=============================================================================
/** It implements a MongoDB client for seismic data 
 *  @author  Rafael Morales Muñoz
 *  @mail    rmorales.iaa.es
 *  @version 1.0
 *  @date    10 Dec 2017
 *  @history None
 */
//=============================================================================
//=============================================================================
// Package section
//=============================================================================
package iceSpark.mongoDB

//=============================================================================
// System import section
//=============================================================================
import com.mongodb.casbah.{ MongoCollection }
import com.mongodb.casbah.Imports._

import org.apache.spark.sql.types._
  
//=============================================================================
// User import section
//=============================================================================
import catSpark.logger.visual.LoggerVisual
import catSpark.database.mongoDB.MyMongoDB
import catSpark.util.MyUtil

//=============================================================================
// Class/Object implementation
//=============================================================================
import MyMongoDB._

class SeismicMDB( dbName: String, collectionName: String ) extends MyMongoDB( dbName, collectionName ) {
  
  //-------------------------------------------------------------------------
  // Constants
  //-------------------------------------------------------------------------
  
  private final val MONGO_ID= "_id"    //long
  
   val schemaTimeStampCol=    "timeStamp"    
   val schemaValueCol =       "value"
   
  val dfSchemaWithMongoID = new StructType()
    .add( StructField( MONGO_ID,        LongType,    false))
    .add( StructField( schemaValueCol,  IntegerType,  false))

  val dfSchema = new StructType()
    .add( StructField( schemaTimeStampCol, LongType,    false))
    .add( StructField( schemaValueCol,    IntegerType,  false))

  //-------------------------------------------------------------------------
  // Code starts
  //-------------------------------------------------------------------------

   connect
     
   //-------------------------------------------------------------------------
   private def bulkInsert(l: List[(Long, Int)]) {
    
    try {                       
      val builder = collection.initializeUnorderedBulkOperation        
      l.map{ case (t: Long, v:Int) => {                        
        builder.insert( MongoDBObject( MONGO_ID -> t.toLong) ++= MongoDBObject( schemaValueCol -> v.toInt) )
      }}                 
      //insert in batch mode
      if (!builder.execute.isAcknowledged) 
        log.error(s"Erro inserting rows in mongoDB: SeismicMDB" )
    } 
    catch {
      case e:Exception => log.error(e.toString)
    } 
  }
  
  //------------------------------------------------------------------
  //Drop the collection first 
  def insert( l: List[(Long, Int)] ) = l.grouped(maxBulkOperationSize) foreach (bulkInsert(_))
  
  //-------------------------------------------------------------------------
  
  import org.apache.spark.sql.{ DataFrame, Row, SQLContext, SparkSession }

  //-------------------------------------------------------------------------
  private def getList( r: MongoCursor ) = {
    
    r.map { obj => 
        val t = obj.get( MONGO_ID ).asInstanceOf[Long]
        val v = obj.get( schemaValueCol ).asInstanceOf[Int]
        (t,v)
      }.toList         
  }
    
  //-------------------------------------------------------------------------  
  private def getDadataFrame( r: MongoCursor ) = {
    
    val l = getList(r) 
    
    sparkSession.createDataFrame(sparkSession.sparkContext.parallelize( l ).map { w => Row( w._1,w._2 ) }    
      , dfSchema )       
  }
     
  //-------------------------------------------------------------------------
  def getAll = super.loadDF( dfSchemaWithMongoID ).toDF(Seq(schemaTimeStampCol, schemaValueCol ): _*)

  //-------------------------------------------------------------------------
  def query(start: Long, end: Long) = {
    
    //query
    val q = new MongoDBObject(MONGO_ID  $gte start   $lte end  )
    
    //sort ascending
    val s = MongoDBObject( MONGO_ID -> 1)
     
    collection.find( q ).sort( s )    
  }

  //-------------------------------------------------------------------------
  def getSignal( start: Long, end: Long ) = {
    
     val q = new MongoDBObject(MONGO_ID  $gte start  $lte end )
     
     //projection
     val p = MongoDBObject( MONGO_ID -> 0 ) ++= MongoDBObject( schemaValueCol -> 1 )  
         
     //sort
     val s =  MongoDBObject( MONGO_ID -> 1) 
       
     //execute the query+projection+sort
     collection.find( q, p )
      .sort( s )
      .map( obj => obj.get( schemaValueCol ).asInstanceOf[Int] ).toList      
  }
  
  //-------------------------------------------------------------------------
  def get(start: Long, end: Long) = getList(query(start,end))
  
  //-------------------------------------------------------------------------
  def getDataFrame(start: Long, end: Long) = getDadataFrame(query(start,end))
  
  //-------------------------------------------------------------------------
  def getMinMaxRangeID = {
    
     val q = new MongoDBObject("_id"  $gte 0)
     
     //projection
     val p = MongoDBObject( schemaValueCol -> 0 ) 
         
     //sort
     val sMin =  MongoDBObject( MONGO_ID -> 1)
     val sMax =  MongoDBObject( MONGO_ID -> -1)
            
    //execute the query+projection+sort
    (collection.findOne( q, p, sMin ).get( MONGO_ID ).asInstanceOf[Long] , 
     collection.findOne( q, p, sMax ).get( MONGO_ID ).asInstanceOf[Long])      
  }
 
  //-------------------------------------------------------------------------
  def importCsv(s:String) = super.importCsv(s, dfSchemaWithMongoID)
  
  //-------------------------------------------------------------------------  
  def saveAsCVS(data: List[(Long,Int)],fileName:String){
        
    import java.io.{ BufferedWriter, File, FileWriter }
    val file = new BufferedWriter( new FileWriter( new File( fileName )))
    file.write(List("timeStamp","value").mkString(",") + MyUtil.LineSeparator)
    data.map(t => {
      file.write(t.productIterator.toList.mkString(",") + MyUtil.LineSeparator)
    })    
             
    file.close
  }
  
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
} //end of class 'SeismicMDB'
//=============================================================================
// End of 'SeismicMDB.scala' file
//=============================================================================
