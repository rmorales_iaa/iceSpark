//=============================================================================
//File: meteoMDB.scala
//=============================================================================
/** It implements a MongoDB client for meteorology data
 *  @author  Rafael Morales Muñoz
 *  @mail    rmorales.iaa.es
 *  @version 1.0
 *  @date    10 Dec 2017
 *  @history None
 */
//=============================================================================
//=============================================================================
// Package section
//=============================================================================
package iceSpark.mongoDB

//=============================================================================
// System import section
//=============================================================================
import com.mongodb.casbah.{ MongoCollection }
import com.mongodb.casbah.Imports._

import org.apache.spark.sql.types._

import org.apache.spark.sql.{ DataFrame, Row, SQLContext, SparkSession }
  
//=============================================================================
// User import section
//=============================================================================
import catSpark.logger.visual.LoggerVisual
import catSpark.database.mongoDB.MyMongoDB
import catSpark.spark.dataFrame.MyDataframe
import catSpark.util.MyUtil

//=============================================================================
// Class/Object implementation
//=============================================================================
import MyMongoDB._

class MeteoMDB( dbName: String, collectionName: String ) extends MyMongoDB( dbName, collectionName ) {
  
  //-------------------------------------------------------------------------
  // Constants
  //-------------------------------------------------------------------------
  
   val mongoConfiguration = getConfiguration
        
   private final val MONGO_ID= "_id"    //long
  
  //-------------------------------------------------------------------------
  // Code starts
  //-------------------------------------------------------------------------

   connect
      
  //-------------------------------------------------------------------------
  private def translateSequence( q: MongoDBObject ) = {
     
     //sort
     val s =  MongoDBObject( MONGO_ID -> 1) 
       
     //execute the query+projection+sort
     collection.find( q )
      .sort( s )
      .map( obj => {(obj.get( MONGO_ID ).asInstanceOf[Long],
                     obj.get( "wind_speed[m/s]" ).asInstanceOf[Double],
                     obj.get( "wind_direction[N_deg]" ).asInstanceOf[Double],
                     obj.get( "wind_speed_x[m/s]" ).asInstanceOf[Double],
                     obj.get( "wind_direction_x[N_deg]" ).asInstanceOf[Double],
                     obj.get( "temperature[c_deg]" ).asInstanceOf[Double],
                     obj.get( "hr[]"  ).asInstanceOf[Double],
                     obj.get( "rain[m3]"  ).asInstanceOf[Double],
                     obj.get( "pressure[mbar]"  ).asInstanceOf[Double],
                     obj.get( "radiation[w_m2]"   ).asInstanceOf[Double],
                     obj.get( "temperature_superficial[c_deg]"  ).asInstanceOf[Double]      
                   )}).toList      
  }
  
  //-------------------------------------------------------------------------
  def get = translateSequence( new MongoDBObject(MONGO_ID  $gte 0 ) )    
    
  //-------------------------------------------------------------------------
  def get( start: Long, end: Long) = translateSequence( new MongoDBObject(MONGO_ID  $gte start  $lte end ) )
  
  //-------------------------------------------------------------------------   
  
              
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
} //end of class 'MeteoMDB'
//=============================================================================
// End of 'meteoMDB.scala' file
//=============================================================================
