//=============================================================================
//File: seismic.scala
//=============================================================================
/** It implements a seismic query
 *  @author  Rafael Morales Muñoz
 *  @mail    rmorales.iaa.es
 *  @version 1.0
 *  @date    6 Sept 2017
 *  @history None
 */
//=============================================================================
//=============================================================================
// Package section
//=============================================================================
package iceSpark.query.seismic

//=============================================================================
// System import section
//=============================================================================
import com.cloudera.sparkts.{DateTimeIndex, DayFrequency, Frequency, HourFrequency, BusinessDayFrequency}
import com.cloudera.sparkts.stats.TimeSeriesStatisticalTests

import java.time.{ LocalDateTime, Instant , ZoneId, ZonedDateTime }
import java.time.format.{ DateTimeFormatter }

import java.io.{ BufferedOutputStream, BufferedWriter, File, FileWriter }

import org.apache.spark.{SparkContext, SparkConf }
import org.apache.spark.sql.{ DataFrame, Row, SQLContext, SparkSession }
import org.apache.spark.sql.types._


//=============================================================================
// User import section
//=============================================================================
import iceSpark.query.Query

import catSpark.logger.visual.LoggerVisual
import catSpark.hadoop.MyHadoop
import catSpark.signal.{ Butter, FFT, SOSFilt }
import catSpark.spark.dataFrame.MyDataframe
import catSpark.util.MyUtil

import iceSpark.mongoDB.{ SeismicMDB, SurroundMDB, LP_MDB, PsdMDB }

//=============================================================================
// Class/Object implementation
//=============================================================================

import Query._ 

object QuerySeismic {
  
  //-------------------------------------------------------------------------
  //Class variables section
  //-------------------------------------------------------------------------
  
  //-------------------------------------------------------------------------
  //schema
      
  val  lpID_Schema = new StructType()
      .add( StructField( "startTimeMs",       LongType,   false))
      .add( StructField( "lp_id",             StringType, false))
      
      .add( StructField( "psdTotal",          DoubleType, false))
      
      .add( StructField( "psdFreqAccum10%",   DoubleType, false))
      .add( StructField( "psdFreqAccum20%",   DoubleType, false))
      .add( StructField( "psdFreqAccum30%",   DoubleType, false))
      .add( StructField( "psdFreqAccum40%",   DoubleType, false))
      .add( StructField( "psdFreqAccum50%",   DoubleType, false))
      .add( StructField( "psdFreqAccum60%",   DoubleType, false))
      .add( StructField( "psdFreqAccum70%",   DoubleType, false))
      .add( StructField( "psdFreqAccum80%",   DoubleType, false))
      .add( StructField( "psdFreqAccum90%",   DoubleType, false))
      
      .add( StructField( "psdFreqAccumRange", DoubleType, false))
  
  //-------------------------------------------------------------------------
  //Coefficients of band pass Butterworth filter according to Matlab. Used in the original filter. Now is discarded
  //See method 'getSurroundingSignalMatlab'    
  //fitler min = 2, max =10
  //Coefficients of band pass Butterworth created with matlab using butter(2/50,10/50)
  val filterNum_2_10_50 = List (0.046131802093311,0,-0.092263604186622,0,0.046131802093311)                  //numerator
  val filterDen_2_10_50 = List(1,-3.174777319452050,3.886581585010506,-2.199123289505815,0.491812237222575)  //denominator
    
  //Coefficients of band pass Butterworth created with matlab using butter(2/50,15/50)
  val filterNum_2_15_50 = List( 0.104078356756097, 0,	-0.208156713512194, 0,	0.104078356756097)             //numerator
  val filterDen_2_15_50 = List( 1, -2.7230533328330, 2.85893747893842, -1.4470372030495, 0.319732469882735)  //denominator
     
  //-------------------------------------------------------------------------  
  //manual lp
  private val manualLP_TimeFormat = DateTimeFormatter.ofPattern("dd'/'MM'/'yyyy'-'HH':'mm':'ss").withZone( MyUtil.zoneID_UTC )
   
  //-------------------------------------------------------------------------  
}
//-------------------------------------------------------------------------

//=============================================================================
//=============================================================================
import QuerySeismic._

import catSpark.configuration.MyConf
import catSpark.spark.MySpark
import com.cloudera.sparkts.TimeSeriesRDD
import iceSpark.mongoDB.SeismicMDB

class QuerySeismic( sp: MySpark
    , sparkSession: SparkSession
    , sqlContext : SQLContext
    , seismicMDB: SeismicMDB) extends Query( sp, sparkSession, sqlContext ) {

  //-------------------------------------------------------------------------
  // Class variable
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
  //time between measure in the original signal
  private val SIGNAL_SAMPLE_MEASURE_IN_MS : Double = 10  
  private val SIGNAL_SAMPLE_FREQUENCY_HZ: Double = 1 / (SIGNAL_SAMPLE_MEASURE_IN_MS / 1000)
  
  //-------------------------------------------------------------------------
  // Code starts
  //-------------------------------------------------------------------------
  
  val surroundMDB = new SurroundMDB(seismicMDB.databaseName, seismicMDB.collectionName+"_sur")
  val lpMDB =       new LP_MDB(seismicMDB.databaseName, seismicMDB.collectionName+"_lp")
  val psdMDB =      new PsdMDB(seismicMDB.databaseName, seismicMDB.collectionName+"_psd")
 
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
  def chi_2_goodness_fit_test(parquetData: String) {
    
     val df = MyDataframe.loadParquetFile( parquetData )      
     sp.chi_2_goodness_fit_test( Some(df) , colIndex_1 = 2 )
  }
  
  //-------------------------------------------------------------------------  
  import org.apache.spark.sql.functions.udf
  
   
  //-------------------------------------------------------------------------
  import catSpark.signal.{ FiltFilt }
  private def filterbandPass (data: List[(Long,Int)]
    ,  num: List[Double]   //filter coefficients for numerator
    ,  den: List[Double]) = {   //filter coefficients for denominator)   
         
    //get the average and subtract it from data to center them    
    val average = data.map(_._2).sum / data.length.toDouble
    val averageData =  data.map(_._2 - average)
    
    //debug
    //val df = MyDataframe.createDataFrameFromArrayDouble(averageData)
    //MyDataframe.saveAsCSV(df, "/mnt/data2TB/vane/test/query/average.csv")
    
    //apply the filter    
    FiltFilt.filtfilt(num, den, averageData).toList    
  }
     
  //-------------------------------------------------------------------------  
  def getSurroundingSignalMatlab (data: List[(Long,Int)] 
    ,  bandPassFilterNum: List[Double]   //filter coefficients for numerator
    ,  bandPassFilterDen: List[Double]
    ,  lowPassFilterOrder: Int = 2
    ,  lowPassFilterCutoffFrequency: Double) = {
          
    //bandpass filter with absolute values
    val filteredBandPass = filterbandPass(data
      ,  bandPassFilterNum 
      ,  bandPassFilterDen)
    
    val absValue = filteredBandPass.map{  Math.abs( _ ) }  
    val surData  = SOSFilt.sosfilt( Butter.butterSOSEven(lowPassFilterOrder, lowPassFilterCutoffFrequency) , absValue )
    
    (data.map(_._1), surData, filteredBandPass).zipped.toList //(timeStamp, surround, passBand)
  }
  
  //-------------------------------------------------------------------------  
  def getSurroundingSignal(data: List[(Long,Int)] 
    ,  bandPassFilterLowHz : Double 
    ,  bandPassFilterHighHz : Double
    ,  lowPassFilterHz : Double) = {
    
    import breeze.linalg.{DenseVector}
    import breeze.numerics._
    import breeze.signal._
    import breeze.stats._
  
    val time = data.map(t=>t._1).toList
    val signal =  DenseVector(data.map(t=>t._2.toDouble):_*)
        
    val centeredSignal = signal - median(signal)
    
    val bandPass = abs(filterBP(abs(centeredSignal)
      ,  omegas=(bandPassFilterLowHz,bandPassFilterHighHz)
      ,  sampleRate=SIGNAL_SAMPLE_FREQUENCY_HZ
      ,  overhang = OptOverhang.PreserveLength 
      ,  padding = OptPadding.Cyclical))
        
    val surroundSignal = abs(filterLP(bandPass
      ,  omega=lowPassFilterHz
      ,  sampleRate=SIGNAL_SAMPLE_FREQUENCY_HZ 
      ,  overhang = OptOverhang.PreserveLength 
      ,  padding = OptPadding.Cyclical))
             
    (time, bandPass.toArray.toList, surroundSignal.toArray.toList).zipped.toList //(timeStamp, surround, passBand)
  }
  
  //-------------------------------------------------------------------------    
  def dateConversion(t: Long) =  MyUtil.getZonedDateTimeStamp(t) 
    
  //-------------------------------------------------------------------------  
  def detectSignal(minTimeMs: Long
    ,  maxTimeMs: Long
    ,  timeToGlueConsecutiveSignalMs: Long = 10) = {
    
    lpMDB.drop
    
     //load the surround data    
     val df = surroundMDB.loadDF
     
     import org.apache.spark.sql.functions._
     import org.apache.spark.sql.expressions.Window
     
     //https://stackoverflow.com/documentation/apache-spark/3903/window-functions-in-spark-sql#t=2017101015091329567
     //add new col with the difference between current row timestamp sample and previous timestamp row sample
     val timeWindow = Window.orderBy( schemaTimeStampCol )
     val nextTimeStamp = lag( col( schemaTimeStampCol ), 1).over( timeWindow )          
     val dfElapsed = df.withColumn("elapsedTime",  df( schemaTimeStampCol) - nextTimeStamp )
  
     //assign a id for each contiguous time observing (10ms between measures)
     var lpID : Long = 0     
     val rdd = dfElapsed.rdd.map { row =>
       
       val elapsedTime = row( 3 ).asInstanceOf[Long] 
       if ( elapsedTime >= timeToGlueConsecutiveSignalMs) lpID += 1  
       Row( row(0), row(1), row(2), lpID )
     }
     
     val dfLPNoRepartition = sparkSession.createDataFrame( rdd, dfElapsed.schema ) //reuse col "elapsedTime" to store the id
     val dfLP = dfLPNoRepartition.repartition( dfLPNoRepartition( "elapsedTime" ) )
        
     //get the contiguous measure time, start time and end time of each LP
     val c = dfLP.groupBy( "elapsedTime" ) 
       .agg( expr( s"count($schemaTimeStampCol)*10-10").as("measureTimeMs")
         , expr( s"min( $schemaTimeStampCol )").as( "startTimeMs" )
          , expr( s"max( $schemaTimeStampCol )").as( "stopTimeMs" ))
       .drop( "elapsedTime" )

     //get only the LP in the range of contiguous measure time
     val lp = c.filter( c("measureTimeMs") >= minTimeMs )
       .filter( c("measureTimeMs") <= maxTimeMs )
       .sort( "startTimeMs" )
       .toDF(Seq("measureTimeMs", "_id", "stopTimeMs"): _*)     //rename startTimeMs to _id
       .select("_id", "stopTimeMs", "measureTimeMs")
       
     //save in MogoDB 
     lpMDB.saveDF_Overwrite(lp)       
  }
  
  //-------------------------------------------------------------------------
  private def detectSignal_eventTimeRange(data: List[(Long, Int)] 
    ,  colName: String
    ,  bandPassFilterLowHz : Double 
    ,  bandPassFilterHighHz : Double
    ,  lowPassFilterHz : Double
    ,  minSourroundAmplitude: Double) = {
         
    //get the surround signal
   // val d = getSurroundingSignalMatlab( data
    val d = getSurroundingSignal( data
      ,  bandPassFilterLowHz 
      ,  bandPassFilterHighHz 
      ,  lowPassFilterHz) //(timeStamp, surround, passBand)
   
    //get the average of the surround data
    val surData = d.map(_._2)  
    val minSur = surData.min 
    val maxSur = surData.max 
    var averageSur = surData.sum / data.length
    val surStd = MyUtil.stdDevList(surData,averageSur)
    
    log.info(s"Min value of surround value in the current slice: $minSur")
    log.info(s"Max surround value: $maxSur")
    log.info(s"Average value of the surround value: $averageSur")
    log.info(s"Standard desviation of the surround value: $surStd")
             
    //Remove values below the max value below the average and below min amplitude
    val r = d.groupBy( _._2 > minSourroundAmplitude)
    if (r.size > 1){      
      r(true)         
      //debug
      //seismicMDB.saveAsCVS(data,"/mnt/data2TB/vane/test/query/raw.csv")
      //surroundMDB.saveAsCVS(d,"/mnt/data2TB/vane/test/query/raw_with_filter.csv")
      //surroundMDB.saveAsCVS(r(true),"/mnt/data2TB/vane/test/query/raw_with_filter_and_avg_filter.csv")                      
    }
    else List()           
  }
  
  //-------------------------------------------------------------------------
  def buildSurroundSignal( timeRangeSeconds: Int 
    ,  bandPassFilterLowHz : Double 
    ,  bandPassFilterHighHz : Double
    ,  lowPassFilterHz : Double
    ,  minSourroundAmplitude: Double) {
              
   log.info("Starting the surround signal process")
   
   surroundMDB.drop
   
   val r = seismicMDB.getMinMaxRangeID  //in ms
   val rangeMinMs = r._1
   val rangeMaxMs = r._2
   val rangeSeconds = (rangeMaxMs - rangeMinMs) / 1000   //in s
       
   log.info(s"Dividing the surround signal in slices of $timeRangeSeconds seconds")
   log.info(s"In the last sclice, data not in time rage will not generate any result")
     
   val sliceCount = (rangeSeconds / timeRangeSeconds).toInt
     
   log.info(s"Start ms: $rangeMinMs")
   log.info(s"End ms: $rangeMaxMs")
   log.info(s"Number of slices: "+(sliceCount+1))
     
   var startMs = rangeMinMs 
   
   for (slice <- 0 to sliceCount) {
          
     val stopMs =  startMs + (timeRangeSeconds * 1000)                   
     val l = seismicMDB.get(startMs, stopMs)
     
     log.info(s"Slice $slice of $sliceCount")
     log.info(s"Time range [$startMs,$stopMs] total points count:" + l.length )
     
     if (l.length > (SIGNAL_SAMPLE_FREQUENCY_HZ * 2)){  
       
       val r = detectSignal_eventTimeRange(l
         ,  schemaIdCol
         ,  bandPassFilterLowHz
         ,  bandPassFilterHighHz
         ,  lowPassFilterHz
         ,  minSourroundAmplitude)
         
        if (r.size > 0 ) surroundMDB.save(r)        
     }
     else log.warning(s"No seismic data in this time range: [$startMs,$stopMs]")
     
     startMs =  startMs + (timeRangeSeconds * 1000)
   }
        
   Query.log.info("End of the surround signal process!!!")
  }
     
  //-------------------------------------------------------------------------
  import catSpark.signal.PSD

  private def getPsd( data: List[Double] ) = {  
   
    //http://faculty.petra.ac.id/resmana/private/matlab-help/toolbox/signal/specgram.html
    //https://github.com/rikrd/matlab/blob/master/signal/signal/specgram.m
    
    //get the power spectral density
    val psd = PSD.psd( data, SIGNAL_SAMPLE_FREQUENCY_HZ ) //power spectral density
        
    //some calculations    
    val psdTotal = psd.map(_._2).sum    
    val accumultedPsdFreq = (for(x <- 1 to 9) yield { PSD.bandwidth( psd, (x * 0.10) ) }).toList
    val range =  accumultedPsdFreq.last - accumultedPsdFreq.head
    
    List ( psdTotal ) :+    //0 
      accumultedPsdFreq :+  //1 
      range                 //2
  } 
    
  //-------------------------------------------------------------------------  
  def assignID_toSignal(outputCSV: String) {
    
    log.info("Starting signal identification")
    
    //connect with database
    import catSpark.database.mongoDB.MyMongoDB
    
    val l = lpMDB.get
    
    val lpCount = l.size
    var currentLP = 0
    var lastPrintedPercentage: Long = -1
   
    if (l.size == 0){
      log.error("No signal detected")
      log.info("End of signal identification")
      return
    }

    //drop current values
    psdMDB.drop
       
    //process all lp, trying to get the form of the signal using PSD
    l.map { row =>

       val start = row._1 
       val end =   row._2
       val data = surroundMDB.getSurroundSignal(start, end).map(_.toDouble)  
       
       if (data.size == 0) log.error( s"LP with start time: $start: has not surround data")
       else{
         //calculate psd and assign ID
         val psdStat= getPsd( data )       
         val psdTotal = psdStat( 0 ).asInstanceOf[Double]
         val accumultedPsdFreq = psdStat( 1 ).asInstanceOf[List[Double]]
         val range = psdStat( 2 ).asInstanceOf[Double]
       
         //apply filter
         var lpIDStringID = "NO_LP"       
         if (psdTotal >= 20000) { //remove low energy events
      
           if ( psdTotal >= 25000 )  {
             if ( range >= 10 ) lpIDStringID = "SP"      
             else lpIDStringID = "LP_HIGH_FEQ"      
           }
           if  (( range <= 3) && (accumultedPsdFreq.head <= 2)) //freq at 10% 
             lpIDStringID = "LP_LOW_FEQ"           
         }           
         
         //save result adding extra info of the lp
         psdMDB.save(MyUtil.recursiveListFlatten( List(start, lpIDStringID) :+ psdStat ),
           List(end, end-start, dateConversion(start),dateConversion(end)))
       }

       //stat
       currentLP += 1          
       val percentage = ( currentLP * 100 / lpCount )
       if (percentage > lastPrintedPercentage){
         log.info (s"Processed lp: $percentage% of $lpCount")
         lastPrintedPercentage = percentage
       }
     } 
        
    //save result for the user
    val outDF = psdMDB.loadDF
    MyDataframe.saveAsCSV(outDF, outputCSV)
    
    log.info("End of signal identification!!!")
  }
  
  //-------------------------------------------------------------------------  
  def closeMongoConnections {    
    
    surroundMDB.close
    lpMDB.close
    psdMDB.close
    seismicMDB.close    
  }  
  
  //-------------------------------------------------------------------------
  def plotJoinData(joinDataCsv: String) = {
    
    //build directories
    val outputDir = MyUtil.ensureEndWithFileSeparator(MyUtil.getParentPath(joinDataCsv))
    MyUtil.ensureDirectoryExist(outputDir)
    val outD = MyUtil.ensureEndWithFileSeparator(outputDir)+"plot/"
    
    MyUtil.ensureDirectoryExist(outD)
        
    val extensionKM_ColName = "extension[Km3]"
    val tideColName = "tide_height[m]"
    val timeColName = "startTimeMs"
    val psdTotalColName = "psdTotal"
    val idColName = "lp_id"
    
    val dfRaw = MyDataframe.loadCSV(joinDataCsv).get
      .select(timeColName, psdTotalColName, tideColName, idColName, extensionKM_ColName)
    
    val df = dfRaw
      .where(dfRaw(tideColName) > 0)
      .where(dfRaw(idColName) =!=  "NO_LP")
     
    //create plot  
    catSpark.plot.MyPlot.create(log 
      ,  df 
      ,  outD
      ,  outD+"draw.plot"
      //,  xColName =  tideColName
      ,  xColName =  psdTotalColName
      ,  y_1_ColNameList = List(extensionKM_ColName) 
      ,  y_2_ColNameList = Seq() )
      
    MyDataframe.saveAsCSV(df, outD+"data.csv")  
  }

  //-------------------------------------------------------------------------  
  private def iceExtensionAggregate(l: List[(Long,Double,Double)]) =  {
    
    if (l.size == 0) (0.toDouble, 0.toDouble)
    else             (l(0)._2, l(0)._3) //avoid _id    
  }

  //-------------------------------------------------------------------------  
  private def meteoAggregate(l: List[(Long,Double,Double,Double,Double,Double,Double,Double,Double,Double,Double)]) =  {
    
    if (l.size == 0) (0.toDouble, 0.toDouble, 0.toDouble, 0.toDouble, 0.toDouble, 0.toDouble, 0.toDouble, 0.toDouble, 0.toDouble, 0.toDouble )
    else             (l.maxBy(_._2)._2, l.maxBy(_._3)._3, l.maxBy(_._4)._4, l.maxBy(_._5)._5, 
                      l.maxBy(_._6)._6, l.maxBy(_._7)._7, l.maxBy(_._8)._8, l.maxBy(_._9)._9, 
                      l.maxBy(_._10)._10, l.maxBy(_._11)._11 ) //avoid _id   
  }  
  
  //-------------------------------------------------------------------------  
  private def tideAggregate(l: List[(Long,Double)]) =  {
    
    if (l.size == 0) (0.toDouble)
    else             (l.maxBy(_._2)._2)    
  }
  //-------------------------------------------------------------------------
  import iceSpark.IceSpark
  import iceSpark.mongoDB.{IceExtensionMDB, MeteoMDB, TideMDB}
  
  def joinData(psdCsv:String, outputCsv:String, importPsdToMongo: Boolean = false) {
        
    //get the min and max timestamp signals to process
    val r = psdMDB.getMinMaxRangeID
    val min = r._1
    val max = r._2    
    log.info( s"Min signal timeStamp: $min ->" + MyUtil.getZonedDateTimeStamp(min))
    log.info( s"Max signal timeStamp: $max ->" + MyUtil.getZonedDateTimeStamp(max))
    
    //get the signal data
    var currentSignal = 0
    val l = psdMDB.get
    val signalCount = l.size
    var lastPrintedPercentage: Long = -1
    log.info( s"Total signals to process: $signalCount")    
    
    //write the header of the output file
    val outFile = new BufferedWriter( new FileWriter( new File( outputCsv )))     
    val header = psdMDB.dfSchema.fields.map { f => f.name }.toList.mkString(",") + 
        "," +iceSpark.data.ice.extension.IceExtension.nameDataList.drop(1).mkString(",") +
         "," +iceSpark.data.meteorology.meteorology.Station.nameDataList.drop(1).mkString(",") +
         ",tide_" + iceSpark.data.tide.tide.Tide.nameDataList.drop(1).mkString(",")+
         ",season"
    outFile.write( header + MyUtil.LineSeparator )
    outFile.flush()
    
    //process all signals    
    l.map { signal =>
      
      val statTime = signal._1
      val stoptTime = signal._2
           
      val iceExtAggregated = iceExtensionAggregate( IceSpark.iceExtensionMDB.get(statTime, stoptTime) )
      val meteoAggregated = meteoAggregate(  IceSpark.meteoMDB.get(statTime, stoptTime) )
      val tideAggregated = tideAggregate(  IceSpark.tideMDB.get(statTime, stoptTime) )
                   
      outFile.write(signal.productIterator.toList.mkString(",") +           
        "," + iceExtAggregated.productIterator.mkString(",") +
        "," + meteoAggregated.productIterator.mkString(",") +
        "," + tideAggregated +
        "," +iceSpark.query.Query.getSeasonID(statTime) + 
        MyUtil.LineSeparator)      
      
      //stat
      currentSignal += 1          
      val percentage = ( currentSignal * 100 / signalCount )
      if (percentage > lastPrintedPercentage){
        log.info (s"Processed signal: $percentage% of $signalCount")
        lastPrintedPercentage = percentage
      }
    }
    
    outFile.flush
    outFile.close
    
    log.info(s"Generated file:  '$outputCsv'")        
  }
  
  //-------------------------------------------------------------------------  
   def saveCSV_SeismicData(csvFile: String,start: Long, end: Long) =  {
      val df = seismicMDB.getDataFrame(start,end)      
      MyDataframe.saveAsCSV(df,csvFile)       
   }
   //-------------------------------------------------------------------------  
   def saveCSV_SurroundData(csvFile: String,start: Long, end: Long) =  {
      val df = surroundMDB.getDataFrame(start,end)      
      MyDataframe.saveAsCSV(df,csvFile)       
   }
   //-------------------------------------------------------------------------  
   def saveCSV_LP_Data(csvFile: String,start: Long, end: Long) =  {
      val df = lpMDB.getDataFrame(start,end)      
      MyDataframe.saveAsCSV(df,csvFile)       
   }   
   //-------------------------------------------------------------------------  
   def saveCSV_PSD_Data(csvFile: String,start: Long, end: Long) =  {
      val df = psdMDB.getDataFrame(start,end)      
      MyDataframe.saveAsCSV(df,csvFile)       
   }
  //------------------------------------------------------------------------- 
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
} //end of case class 'Seismic'
//=============================================================================
// End of 'seismic.scala' file
//=============================================================================

