//=============================================================================
//File: hk.scala
//=============================================================================
/** It implements a housekeeping task
 *  @author  Rafael Morales Muñoz
 *  @mail    rmorales.iaa.es
 *  @version 1.0
 *  @date    9 Nov 2016
 *  @history None
 */
//=============================================================================
//=============================================================================
// Package section
//=============================================================================
package iceSpark.query

//=============================================================================
// System import section
//=============================================================================
import java.time.{ Instant, LocalDateTime, Month, ZonedDateTime, ZoneId }
import java.time.format.{ DateTimeFormatter }
import org.apache.spark.sql.{ DataFrame, SQLContext, SparkSession, Row }

//=============================================================================
// User import section
//=============================================================================
import catSpark.logger.visual.LoggerVisual
import catSpark.spark.MySpark
import catSpark.spark.dataFrame.MyDataframe
import catSpark.util.MyUtil

//=============================================================================
// Class/Object implementation
//=============================================================================

object Query {
  
  //-------------------------------------------------------------------------
  //Class variables section
  //-------------------------------------------------------------------------
  var log : LoggerVisual = null
  
  //-------------------------------------------------------------------------
  //Time management   
  
  val YearList =  List(2008, 2009 , 2010, 2011, 2012, 2013, 2014, 2015, 2016)
  var seasonByYear = scala.collection.mutable.Map[Int, List[(String, Long, Long)] ]() //(seasonName,start,end)
  
  //-------------------------------------------------------------------------
  //dataframe
  val schemaTimeStampCol=    "timeStamp"  
  val schemaIdCol    =       "id"  
  val schemaValueCol =       "value"
  val schemaAbsBandPassCol = "absPassBand_value"
  val schemaBandPassCol =    "passBand_value"
  val schemaSurroundCol =    "surround_value"  
  val schemaAmplitude =      "amplitude"
  val schemaFrequency =      "frequency"
  val schemaLP_ID =          "lpID"
  
  //-------------------------------------------------------------------------      
  def initSeasonByYear {
    
    for {
      year <- YearList
    }
    seasonByYear += (year -> MyUtil.seasonAntarticaDateRangeList.map( season => 
                      ( season._1
                         , MyUtil.getTimeStamp( year+"-"+season._2, MyUtil.seasonFormatter)
                         , MyUtil.getTimeStamp( year+"-"+season._3, MyUtil.seasonFormatter) )) 
                      )
  }
      
  //-------------------------------------------------------------------------    
  def getSeasonID(ms : Long
    , prefix: String =""
    , suffix: String ="") : String = {
     
    val l  =  seasonByYear.get( MyUtil.getZonedDateTimeTimeStamp( ms ).getYear ).get
    l.foreach( range =>
      if ( ( ms >= range._2 )   //start 
        && ( ms <= range._3 ))  //end
        return prefix + range._1 + suffix      
    )
    "NONE_SEASON"        
  }
  
  //-------------------------------------------------------------------------
}

//=============================================================================
//=============================================================================
import Query._

abstract class Query( sp: MySpark, sparkSession: SparkSession, sqlContext : SQLContext ) {

  //-------------------------------------------------------------------------
  // Code starts


  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
} //end of case class 'Query'
//=============================================================================
// End of 'query.scala' file
//=============================================================================
