//=============================================================================
//File: icebergPosition.scala
//=============================================================================
/** It implements a data manager for iceberg position data
 *  @author  Rafael Morales Muñoz
 *  @mail    rmorales.iaa.es
 *  @version 1.0
 *  @date    24 Aug 2016
 *  @history None
 */
//=============================================================================
//=============================================================================
// Package section
//=============================================================================
package iceSpark.data.iceberg.position

//=============================================================================
// System import section
//=============================================================================
import java.util.Date
import java.io.File

import org.apache.spark.sql.{DataFrame, Row, SparkSession}
import org.apache.spark.sql.types._

import org.joda.time.{ DateTime, DateTimeZone }
import org.joda.time.format.DateTimeFormat

//=============================================================================
// User import section
//=============================================================================
import catSpark.configuration.MyConf
import catSpark.logger.visual.LoggerVisual
import catSpark.spark.dataFrame.MyDataframe
import catSpark.util.MyUtil

//=============================================================================
// Class/Object implementation
//=============================================================================
object IcebergPosition {
  //-------------------------------------------------------------------------
  //Class variables section
  //-------------------------------------------------------------------------
  var log : LoggerVisual = null
  var sparkSession: SparkSession = null

  //-------------------------------------------------------------------------  
  //configuration  
  private val InputPath =  MyUtil.ensureEndWithFileSeparator(MyConf.getString( "IceSpark.data.iceberg.rootInputPath" ))
  private val OutputPath = MyUtil.ensureEndWithFileSeparator(MyConf.getString( "IceSpark.data.iceberg.rootoOutPath" ))

  //-------------------------------------------------------------------------
  //dataframe
  private val DataTimeFormat = DateTimeFormat.forPattern("yyyy:MM:dd:HH:mm").withZone(DateTimeZone.UTC)
  
  private val schema = new StructType()
    .add( StructField( "timeStamp[ms]" ,            LongType,  false )) 
    .add( StructField( "latitude[dec_deg]" ,        FloatType, false ))
    .add( StructField( "longitude",                 FloatType, false ))
    .add( StructField( "orientation[radians]",      FloatType, false ))
    .add( StructField( "bottom_temperature[C_deg]", FloatType, false ))
    .add( StructField( "top_temperature[C_deg]",    FloatType, false ))
    .add( StructField( "relative_humidity[%]",      FloatType, false ))
    .add( StructField( "wind_direction[radians]",   FloatType, false ))
    .add( StructField( "wind_speed[m_s]",           FloatType, false ))
    .add( StructField( "pressure[mbar]",            FloatType, false ))

  //-------------------------------------------------------------------------
  //time range
  private var startDate: Long = 0
  private var endDate: Long = 0
    
  //-------------------------------------------------------------------------  
  //file extension  
  private val FileExtensionData = ".txt"
  private val ValidFileExtension = List (FileExtensionData)  

  //-------------------------------------------------------------------------
  // Code starts
  //-------------------------------------------------------------------------
  
    //-------------------------------------------------------------------------
  private def createDataFrame(l : List[(Long, Float, Float, Float, Float, Float, Float, Float, Float, Float) ]) : DataFrame = {
        
    val rdd = sparkSession.sparkContext.parallelize(l).map { r => Row( r._1, r._2, r._3, r._4, r._5, r._6, r._7, r._8, r._9, r._10)  }    
    sparkSession.createDataFrame( rdd, schema )  
  }
  
  //-------------------------------------------------------------------------
  private def translateFile( f: File ) {
    import scala.io.Source
    
    val startTime = System.currentTimeMillis
    val fFull = f.getAbsolutePath
    val fName = MyUtil.getPathOnlyFilename(fFull)
    val parent = MyUtil.getParentPath(fFull)
    
    log.info(s"Processing file '$fName'")
    
    //parse all lines
    val l = Source.fromFile(fFull).getLines().toList.map { line =>
      val split = line.split(",").map(_.trim)
      if ( (split.size >= schema.size) && (split(0).forall(_.isDigit) ) ){  //ignore  comments
        
        val date = DataTimeFormat.parseDateTime( split.take( 5 ).mkString( ":" ) ).getMillis
         
        if  ( (date >= startDate) && (date <= endDate)){
          Some( date 
              , split(5).toFloat
              , split(6).toFloat
              , split(7).toFloat
              , split(8).toFloat
              , split(9).toFloat
              , split(10).toFloat
              , split(11).toFloat
              , split(12).toFloat
              , split(13).toFloat
              )
        }
        else None  
      }
      else None                         
    }.filter(_.isDefined).flatten

    MyDataframe.saveDataframeAsParquet( createDataFrame ( l ), OutputPath+parent, fName )
        
    log.info("Elapsed time:" + MyUtil.getString(System.currentTimeMillis-startTime))
  }  
  //-------------------------------------------------------------------------
  private def translateDirectory( baseDir:  String ) {

    MyUtil.getSortFileList(baseDir,ValidFileExtension).foreach { translateFile( _ ) }      
     
    MyUtil.getSortSubDirectoryList( baseDir ).foreach{ d => 
      MyUtil.getSortFileList(d.getAbsolutePath,ValidFileExtension).foreach { translateFile( _ ) }      
      translateDirectory(d.getAbsolutePath) //recursive call
    }    
  }
  
  //-------------------------------------------------------------------------
  def translateDirectory(_startDate: DateTime, _endDate: DateTime){
        
    //set the range date  
    startDate = _startDate.getMillis
    endDate = _endDate.getMillis

    translateDirectory(InputPath)
  }
  
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
} //end of object 'IcebergPosition'
//=============================================================================
// End of 'icebergPosition.scala' file
//=============================================================================
