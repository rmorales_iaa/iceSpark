//=============================================================================
//File: sextractor.scala
//=============================================================================
/** It implements a wrapper for ffts
 * https://github.com/anthonix/ffts
 *  To generate the C header file. Run in the root of the project: sbt javah
 *  The c header file is generated in: /target/native/include/   
 *  @author  Rafael Morales Muñoz
 *  @mail    rmorales.iaa.es
 *  @version 1.0
 *  @date    7 Aug 2016
 *  @history None
 */
//=============================================================================
//=============================================================================
// Package section
//=============================================================================
package iceSpark.wrapper

//=============================================================================
// System import section
//=============================================================================
import ch.jodersky.jni.nativeLoader

//=============================================================================
// User import section
//=============================================================================
import catSpark.util.MyUtil

//=============================================================================
// Class/Object implementation
//=============================================================================

object fftsNative {

  //-------------------------------------------------------------------------
  
  System.load(MyUtil.getCurrentPath +"native/libfftsNative.so")

  //-------------------------------------------------------------------------
  
  def calculate(input: Array[Float]) = {
    
    val inputComplex = input.map( (_,0.0)) flatMap (x => List(x._1.toFloat, x._2.toFloat))
    val outputComplex= new Array[Float](inputComplex.size)
    
    run(inputComplex, outputComplex, input.size)
    outputComplex
    
  }
  
  //-------------------------------------------------------------------------
  @native def run(input: Array[Float]
    , output: Array[Float]
    , itemCount : Int): Int
    
  //-------------------------------------------------------------------------
}
//-------------------------------------------------------------------------
//end of object 'Sextractor'
//=============================================================================
// End of 'sextractor.scala' file
//=============================================================================
