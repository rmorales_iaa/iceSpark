//=============================================================================
//File: seismicMiniSeed.scala
//=============================================================================
/** Seismic data in miniSeed format manager
 *  Based on "javaSeed.jar" library created using:
 *  https://github.com/crotwell/seedCodec
  	https://github.com/crotwell/seisfile
  	http://www.seis.sc.edu/seisFile.html
  	http://www.seis.sc.edu/seedCodec.html
 *  @author  Rafael Morales Muñoz
 *  @mail    rmorales.iaa.es
 *  @version 1.0
 *  @date    9 Nov 2016
 *  @history None
 */
//=============================================================================
//=============================================================================
// Package section
//=============================================================================

package iceSpark.data.seismic.seismic

//=============================================================================
// System import section
//=============================================================================
import java.io.File
import java.sql.Timestamp
import java.time.{ Instant , ZoneId, ZonedDateTime }
import java.util.Date

import org.apache.spark.sql.{DataFrame, Row, SparkSession}
import org.apache.spark.sql.types._

import org.joda.time.{ DateTime, DateTimeZone }
import org.joda.time.format.DateTimeFormat

//=============================================================================
// User import section
//=============================================================================
import catSpark.configuration.MyConf
import catSpark.logger.visual.LoggerVisual
import catSpark.spark.dataFrame.MyDataframe
import catSpark.util.MyUtil

import iceSpark.mongoDB.SeismicMDB

//=============================================================================
// Code section
//=============================================================================

object SeismicMiniSeed{
  
  //-------------------------------------------------------------------------
  //Class variables
  //-------------------------------------------------------------------------
  var log : LoggerVisual = null
  var sparkSession: SparkSession = null
  
  //-------------------------------------------------------------------------
  //dataframe   
  val ComponentAbbrevationList = List( "E", "N", "Z" )
    
  val timeSerieDateCol  = "date"  
  val timeSerieIdCol    = "id"
  val timeSerieValueCol = "value"
  
  val zoneID  = ZoneId.of("UTC")
  
  //file extension  
  private val FileExtensionData = ".csv"
  private val ValidFileExtension = List ( FileExtensionData )
  
  //dataframe schema  
  val schemaTimeStampCol=    "timeStamp"  
  val schemaValueCol =       "value"
  private val seismicRawSchema = new StructType()
    .add( StructField( schemaTimeStampCol,  LongType,    false))
    .add( StructField( schemaValueCol,      IntegerType,  false))
    
  //seimicMDB
  var seismicMDB: SeismicMDB = null

  //-------------------------------------------------------------------------
  //time range
  private var startDate: Long = 0
  private var endDate: Long = 0
  
  //-------------------------------------------------------------------------  
  //file extension  
  val FileExtension_1_EastComponent     = ".L-E"
  val FileExtension_1_NorthComponent    = ".L-N"  
  val FileExtension_1_VerticalComponent = ".L-Z"
  
  val FileExtension_2_EastComponent     = ".LHE"
  val FileExtension_2_NorthComponent    = ".LHN"  
  val FileExtension_2_VerticalComponent = ".LHZ"
  
  val FileExtensionEast =     List( FileExtension_1_EastComponent,     FileExtension_2_EastComponent )
  val FileExtensionNorth =    List( FileExtension_1_NorthComponent,    FileExtension_2_NorthComponent ) 
  val FileExtensionVertical = List( FileExtension_1_VerticalComponent, FileExtension_2_VerticalComponent )
    
  //-------------------------------------------------------------------------
  private def translateFile(s: String) : (Boolean ,List[Int], Long, Int) = {

    import javaSeed.Main.parseMiniSeedFileIntData;
    import scalaj.collection.Imports._
    					
		val sampleList = new java.util.ArrayList[Integer]()
		val startTime =  Array[Long](-1)
		val sampleRate = Array[Int](-1)
		val r = javaSeed.Main.parseMiniSeedFileIntData(s
		  ,  sampleList
		  ,  startTime
		  ,  sampleRate)	  
		(r
		 ,sampleList.asScala.toList
		 ,startTime(0)
		 ,sampleRate(0))
	}
  
  //-------------------------------------------------------------------------
  private def createDataFrame( a: List[(Long, Int)] ) : DataFrame = {
    
    val rdd = sparkSession.sparkContext.parallelize(a).map { 
      r => Row(  r._1.toLong
               , r._2.toInt ) 
    }     
    sparkSession.createDataFrame( rdd, seismicRawSchema )      
  }
  
  //-------------------------------------------------------------------------
   def getDataFromMiniSeed( s: String ) :  Option[List[(Long, Int)]] = {
    
    val fname = MyUtil.getPathOnlyFilename(s)
    log.info(s"Translating from miniSeed format the file: '$fname'")
    
    val r = translateFile(s)
    if (r._1){

      val sampleList = r._2
      val startTimeStamp = r._3
      val sampleRateMs = (1000 / r._4) //Sample rate per second
		  val sampleSize = sampleList.size
		  val endTimeStamp = startTimeStamp + ((sampleSize -1) * sampleRateMs)
		  
		  
		  log.info( s"Using time range [$startTimeStamp,$endTimeStamp] taking measure each: $sampleRateMs ms and measure count: $sampleSize")
		  
		  val ratioMs = 1000 / r._4
      Some(sampleList.zipWithIndex.map { case (s,i) => (( startTimeStamp + (i*sampleRateMs) ) , s ) })
    }
    else None    
  }
  
  //-------------------------------------------------------------------------
  private def checkFile(s: String) : Boolean = {
    
    if (!MyUtil.fileExist(s)) return log.error(s"File '$s' does not exits")
    if (new File(s).length() <= 10) return log.error(s"File '$s' is to short")
    true
  }
  
  //-------------------------------------------------------------------------
  private def translateSimpleFile( s: String ) : Boolean = {
        
     //check files
     if ( !checkFile( s ) ) return false
          
     //add to big csv file  
     val r = getDataFromMiniSeed( s )
     if (r.isDefined) seismicMDB.insert(r.get)      
     true
  }
  
  //-------------------------------------------------------------------------
  private def trasnlateFileList (l: List[File] 
    ,  componentToParse: Int ) {
         
    l.foreach { f => translateSimpleFile( f.getAbsolutePath ) }
  }  
  
  //-------------------------------------------------------------------------
  def insertParquetToMongoDB(inputParquet: String ) {
    
     val df = MyDataframe.loadParquetFile( inputParquet )
     .drop("id")
     .toDF(Seq("_id", "value" ): _*)
     .cache
          
     seismicMDB.saveDF_Overwrite(df)
  }
  
  //-------------------------------------------------------------------------
  private def translateDir( baseDir: String,  fileExtension: List[String], componentToParse: Int ){

    log.info( "Parsing root directory:" +  baseDir )
    
    val d = new File (baseDir)
    fileExtension.foreach( ext => {                    
      
      val fileList =  MyUtil.getSortFileList( d.getAbsolutePath, ext )      
      if ( fileList.size > 0){ 
        trasnlateFileList(fileList, componentToParse)
        log.info(s"End of translating directory '$d'")
      }
     })
     
    MyUtil.getSortSubDirectoryList(baseDir).foreach{ dir => translateDir ( dir.getAbsolutePath, fileExtension, componentToParse ) }
  }
    
  //-------------------------------------------------------------------------
  def translate( inputMiniSeedRootPath: String            
    ,  station: String
    ,  year: String
    , _seismicMDB: SeismicMDB
    , _startDate: DateTime
    , _endDate: DateTime   
    ,  componentToParse: Int = 4) = { //1 for E-component, 2 for N-component, 4 for Z component, and logical combinations (5 for N and E)
    
    //set the MDBcreate
    seismicMDB = _seismicMDB
    
    //set the range date  
    startDate = _startDate.getMillis
    endDate = _endDate.getMillis

    //get the file filters    
    val r = (componentToParse & 4)
    var fileExtension = new  scala.collection.mutable.ListBuffer[String]()
    if ((componentToParse & 1) == 1) fileExtension ++= FileExtensionEast
    if ((componentToParse & 2) == 2) fileExtension ++= FileExtensionNorth
    if ((componentToParse & 4) == 4) fileExtension ++= FileExtensionVertical
    
   // outputMiniSeedBaseName = _outputMiniSeedBaseName
      
    val dir = inputMiniSeedRootPath + station + MyUtil.FileSeparator + year + MyUtil.FileSeparator
    if (MyUtil.directoryExist( dir ))
      translateDir( dir, fileExtension.toList, componentToParse )
  }
  
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
} //End of object 'SeismicMiniSeed'

//=============================================================================
// End of 'seismicMiniSeed.scala' file
//=============================================================================
