//=============================================================================
//File: iceExtensionMDB.scala
//=============================================================================
/** It implements a MongoDB client for ice extension
 *  @author  Rafael Morales Muñoz
 *  @mail    rmorales.iaa.es
 *  @version 1.0
 *  @date    10 Dec 2017
 *  @history None
 */
//=============================================================================
//=============================================================================
// Package section
//=============================================================================
package iceSpark.mongoDB

//=============================================================================
// System import section
//=============================================================================
import com.mongodb.casbah.{ MongoCollection }
import com.mongodb.casbah.Imports._

import org.apache.spark.sql.types._

import org.apache.spark.sql.{ DataFrame, Row, SQLContext, SparkSession }
  
//=============================================================================
// User import section
//=============================================================================
import catSpark.logger.visual.LoggerVisual
import catSpark.database.mongoDB.MyMongoDB
import catSpark.spark.dataFrame.MyDataframe
import catSpark.util.MyUtil

//=============================================================================
// Class/Object implementation
//=============================================================================
import MyMongoDB._

class IceExtensionMDB( dbName: String, collectionName: String ) extends MyMongoDB( dbName, collectionName ) {
  
  //-------------------------------------------------------------------------
  // Constants
  //-------------------------------------------------------------------------
  
   val mongoConfiguration = getConfiguration
        
   private final val MONGO_ID= "_id"    //long
  
  //-------------------------------------------------------------------------
  // Code starts
  //-------------------------------------------------------------------------

   connect
      
  //-------------------------------------------------------------------------
  private def translateSequence( q: MongoDBObject ) = {
     
     //sort
     val s =  MongoDBObject( MONGO_ID -> -1) 
     
     val fielList = MongoDBObject("_id" -> 1) ++= 
                    MongoDBObject("extension[Km3]" -> 1) ++= 
                    MongoDBObject("extensionMissing[Km3]" -> 1) 
       
     //execute the query+projection+sort
     collection.findOne( q, fielList, s )
       .map( obj => {(obj.get( MONGO_ID ).asInstanceOf[Long],
                     obj.get( "extension[Km3]" ).asInstanceOf[Double],
                     obj.get( "extensionMissing[Km3]" ).asInstanceOf[Double]
                   )}).toList
  }
  
  //-------------------------------------------------------------------------
  def get = translateSequence( new MongoDBObject(MONGO_ID  $gte 0 ) )    
    
  //-------------------------------------------------------------------------
  def get( start: Long, end: Long) = translateSequence( new MongoDBObject(MONGO_ID  $lte start ) )
  
  //-------------------------------------------------------------------------   
  
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
} //end of class 'IceExtensionMDB'
//=============================================================================
// End of 'iceExtensionMDB.scala' file
//=============================================================================
