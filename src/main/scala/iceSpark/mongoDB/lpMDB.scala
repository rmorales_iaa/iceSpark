//=============================================================================
//File: lpMDB.scala
//=============================================================================
/** It implements a MongoDB client for seismic lp data 
 *  @author  Rafael Morales Muñoz
 *  @mail    rmorales.iaa.es
 *  @version 1.0
 *  @date    10 Dec 2017
 *  @history None
 */
//=============================================================================
//=============================================================================
// Package section
//=============================================================================
package iceSpark.mongoDB

//=============================================================================
// System import section
//=============================================================================
import com.mongodb.casbah.{ MongoCollection }
import com.mongodb.casbah.Imports._

import org.apache.spark.sql.types._

import org.apache.spark.sql.{ DataFrame, Row, SQLContext, SparkSession }
  
//=============================================================================
// User import section
//=============================================================================
import catSpark.logger.visual.LoggerVisual
import catSpark.database.mongoDB.MyMongoDB
import catSpark.spark.dataFrame.MyDataframe
import catSpark.util.MyUtil

//=============================================================================
// Class/Object implementation
//=============================================================================
import MyMongoDB._

class LP_MDB( dbName: String, collectionName: String ) extends MyMongoDB( dbName, collectionName ) {
  
  //-------------------------------------------------------------------------
  // Constants
  //-------------------------------------------------------------------------
  
   val mongoConfiguration = getConfiguration
        
   private final val MONGO_ID= "_id"    //long
  
   val schemaTimeStampCol=    "timeStamp"  
   val schemaMeasureTime =  "measureTimeMs"  
   val schemaStartTimeMs =  "startTimeMs"  
   val schemaStopTimeMs =   "stopTimeMs"

   val dfSchemaWithMongoID = new StructType()      
      .add( StructField( MONGO_ID,            LongType, false))
      .add( StructField( schemaStopTimeMs,    LongType, false))
      .add( StructField( schemaMeasureTime,   LongType, false))
      
  val dfSchema = new StructType()
      .add( StructField( schemaTimeStampCol,  LongType, false))
      .add( StructField( schemaStopTimeMs,    LongType, false))
      .add( StructField( schemaMeasureTime,   LongType, false))
      
   private final val FIELD_ID= "_id"    //long
   
  //-------------------------------------------------------------------------
  // Code starts
  //-------------------------------------------------------------------------

   connect
   
  //-------------------------------------------------------------------------   
  def loadDF = super.loadDF( dfSchemaWithMongoID ).toDF( Seq(schemaStartTimeMs, schemaStopTimeMs, schemaMeasureTime): _*)
   
  //-------------------------------------------------------------------------
  private def query( q: MongoDBObject ) = {
    
     //projection
     val p = MongoDBObject( schemaMeasureTime -> 0 ) 
     
     //sort
     val s =  MongoDBObject( MONGO_ID -> 1) 
       
     //execute the query+projection+sort
     collection.find( q, p )
      .sort( s )
      .map( obj => {(obj.get( MONGO_ID ).asInstanceOf[Long],
                     obj.get( schemaStopTimeMs ).asInstanceOf[Long])}).toList      
  }
  
  //-------------------------------------------------------------------------
  def get = query( new MongoDBObject(MONGO_ID  $gte 0 ) )    
    
  //-------------------------------------------------------------------------   
  def saveDF(outputCSV: String) = MyDataframe.saveAsCSV(loadDF, outputCSV)
  
  //-------------------------------------------------------------------------
  private def getList( r: MongoCursor ) = {
    
    r.map { obj => 
        val t = obj.get( MONGO_ID ).asInstanceOf[Long]
        val s = obj.get( schemaStopTimeMs ).asInstanceOf[Long]
        val p = obj.get( schemaMeasureTime ).asInstanceOf[Long]
        (t,s,p)
      }.toList         
  }
  
  //-------------------------------------------------------------------------
  def getDataFrame(start: Long, end: Long) = {
    
     val q = new MongoDBObject(MONGO_ID  $gte start  $lte end )
         
     //sort
     val s =  MongoDBObject( MONGO_ID -> 1) 
       
     //execute the query+sort
     getDadataFrame(collection.find( q ).sort( s ))     
  }
  
  //-------------------------------------------------------------------------  
  private def getDadataFrame( r: MongoCursor ) = {
    
    val l = getList(r) 
    
    sparkSession.createDataFrame(sparkSession.sparkContext.parallelize( l ).map { w => Row( w._1, w._2, w._3 ) }    
      , dfSchema )       
  }
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
} //end of class 'LP_MDB'
//=============================================================================
// End of 'lpMDB.scala' file
//=============================================================================
