//=============================================================================
//File: station.scala
//=============================================================================
/** It implements a data manager for a station metereology data
 *  @author  Rafael Morales Muñoz
 *  @mail    rmorales.iaa.es
 *  @version 1.0
 *  @date    24 Aug 2016
 *  @history None
 */
//=============================================================================
//=============================================================================
// Package section
//=============================================================================
package iceSpark.data.meteorology.meteorology

//=============================================================================
// System import section
//=============================================================================
import java.util.Date
import java.io.File
import java.util.Locale

import org.apache.spark.sql.{DataFrame, Row, SparkSession}
import org.apache.spark.sql.types._

import org.joda.time.format.DateTimeFormatter

//=============================================================================
// User import section
//=============================================================================
import catSpark.configuration.MyConf
import catSpark.database.mongoDB.MyMongoDB
import catSpark.logger.visual.LoggerVisual
import catSpark.office.apache.poi.Poi
import catSpark.spark.dataFrame.MyDataframe
import catSpark.util.MyUtil

//=============================================================================
// Class/Object implementation
//=============================================================================

//-------------------------------------------------------------------------  
//Stations enumerantion  
//-------------------------------------------------------------------------
sealed trait StationType

object StationType {
  
  case object Dcp extends StationType //station at Deception isle
  case object Lvn extends StationType //station at Livingston isle 
  case object Ccv extends StationType //station at Caleta Cierva station       
  
} //end of object 'StationType'

//=============================================================================

object Station {
  //-------------------------------------------------------------------------
  //Class variables section
  //-------------------------------------------------------------------------
  var log : LoggerVisual = null
  var sparkSession: SparkSession = null
  
  //-------------------------------------------------------------------------
  //time range
  var startDate: Long = 0
  var endDate: Long = 0
  
  //-------------------------------------------------------------------------
  //dataframe  
  val schema = new StructType()
    .add(StructField("timeStamp[ms]" ,                   LongType,  false))  //0
    .add(StructField("wind_speed[m/s]",                  FloatType, false))  //1
    .add(StructField("wind_direction[N_deg]",            FloatType, false))  //2
    .add(StructField("wind_speed_x[m/s]",                FloatType, false))  //3
    .add(StructField("wind_direction_x[N_deg]",          FloatType, false))  //4
    .add(StructField("temperature[c_deg]",               FloatType, false))  //5
    .add(StructField("hr[]",                             FloatType, false))  //6
    .add(StructField("rain[m3]",                         FloatType, false))  //7
    .add(StructField("pressure[mbar]",                   FloatType, false))  //8
    .add(StructField("radiation[w_m2]",                  FloatType, false))  //9
    .add(StructField("temperature_superficial[c_deg]",   FloatType, false))  //10
    
  val nameDataList = "_id" :: (schema.fields.map { f => f.name }.drop(1).toList)
   
  //-------------------------------------------------------------------------  
  //file extension  
  private val FileExtensionData = ".csv" 
  private val ValidFileExtension = List (FileExtensionData)
  
  
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
} //end of object 'StationMeteorology'

//=============================================================================

import Station._

//-------------------------------------------------------------------------
class Station (inputPath: String
  ,  outputPath: String
  ,  dateFormat: DateTimeFormatter  
  ,  stationType: StationType) {
    
  //-------------------------------------------------------------------------
  // Code starts
  //-------------------------------------------------------------------------

//-------------------------------------------------------------------------
  private def createDataFrame(l : List[(Long, List[Float]) ]) : DataFrame = {
      
    val rdd = sparkSession.sparkContext.parallelize(l).map { r => Row( r._1 , 
      r._2(0), r._2(1), r._2(2), r._2(3), r._2(4), r._2(5), r._2(6), r._2(7), r._2(8), r._2(9)) }    
    sparkSession.createDataFrame( rdd, schema )  
  }
  
  //-------------------------------------------------------------------------
  private def getStringAsInt(s: Option[String]) : Int = {
    
    if  ((s.isEmpty) || (s.get.isEmpty)) "0".toInt
    else s.get.toInt
  }
  
  //-------------------------------------------------------------------------
  private def getStringAsFloat(s: Option[String]) : Float = {
     
    if  ((s.isEmpty) || (s.get.isEmpty)) "0".toFloat
    else s.get.toFloat
  }
  
  //-------------------------------------------------------------------------
   def getZeroWhenEmpty(s: String) : String = {
  
     if ( (s == null) || (s.isEmpty) ) return "0"
     s.trim
  }
  
  //-------------------------------------------------------------------------
  private def translateFile( f: File ) {
    import scala.io.Source
    import org.joda.time.format.DateTimeFormat
   
    val startTime = System.currentTimeMillis
    val fFull = f.getAbsolutePath
    val fName = MyUtil.getPathOnlyFilename(fFull)
    val parent = MyUtil.getParentPath(fFull)
    
    log.info(s"Processing file '$fName'")
    
    //parse all lines
    val dataColCount = schema.size -1 
    val l = Source.fromFile(fFull).getLines().toList.map { line =>
      
      if (line.startsWith( "id" )) None  //avoid header
      else {
        val split = line.split(",", -1).map( getZeroWhenEmpty( _ ) )        
        if (split.size > (dataColCount +2)){
          val date = dateFormat.parseDateTime( split.drop( 1 ).take( 2 ).mkString( ":" ) ).getMillis         
          if  ( (date >= startDate) && (date <= endDate) )    
            Some( date , 
                  split.drop(3).map (_.toFloat ).take( dataColCount ).toList )          
          else None  
        }
        else None            
      }       
    }.flatten
    
    val df = createDataFrame ( l )
    StationMeteorology.mongoConnection.saveDF_Overwrite(df.toDF(nameDataList.toSeq:_*))
    
    //this creates the parquet file
    //MyDataframe.saveDataframeAsParquet( createDataFrame ( l ), outputPath+parent, fName )
        
    log.info("Elapsed time:" + MyUtil.getString(System.currentTimeMillis-startTime))
  }  
  
  //-------------------------------------------------------------------------
  def translateDirectory( baseDir:  String = inputPath ) {

    MyUtil.getSortFileList(baseDir,ValidFileExtension).foreach { translateFile( _ ) }      
     
    MyUtil.getSortSubDirectoryList( baseDir ).foreach{ d => 
      MyUtil.getSortFileList(d.getAbsolutePath,ValidFileExtension).foreach { translateFile( _ ) }      
      translateDirectory(d.getAbsolutePath) //recursive call
    }    
  }
  
  //-------------------------------------------------------------------------
} //end of case class 'Station'

//-------------------------------------------------------------------------
//-------------------------------------------------------------------------

//=============================================================================
// End of 'station.scala' file
//=============================================================================
