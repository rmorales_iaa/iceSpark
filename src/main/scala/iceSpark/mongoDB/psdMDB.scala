//=============================================================================
//File: psdMDB.scala
//=============================================================================
/** It implements a MongoDB client for seismic psd (power spectrum density) data 
 *  @author  Rafael Morales Muñoz
 *  @mail    rmorales.iaa.es
 *  @version 1.0
 *  @date    10 Dec 2017
 *  @history None
 */
//=============================================================================
//=============================================================================
// Package section
//=============================================================================
package iceSpark.mongoDB

//=============================================================================
// System import section
//=============================================================================
import com.mongodb.casbah.{ MongoCollection }
import com.mongodb.casbah.Imports._

import org.apache.spark.sql.types._

import org.apache.spark.sql.{ DataFrame, Row, SQLContext, SparkSession }
  
//=============================================================================
// User import section
//=============================================================================
import catSpark.logger.visual.LoggerVisual
import catSpark.database.mongoDB.MyMongoDB
import catSpark.spark.dataFrame.MyDataframe
import catSpark.util.MyUtil

//=============================================================================
// Class/Object implementation
//=============================================================================
import MyMongoDB._

class PsdMDB( dbName: String, collectionName: String ) extends MyMongoDB( dbName, collectionName ) {
  
  //-------------------------------------------------------------------------
  // Constants
  //-------------------------------------------------------------------------
  
   val mongoConfiguration = getConfiguration
        
   private final val MONGO_ID= "_id"    //long
  
     
   val schemaStartTimeMs =  "startTimeMs"  
   val schemaStopTimeMs =   "stopTimeMs"
   val schemaMeasureTime =  "measureTimeMs"

   val dfSchemaWithMongoID = new StructType()      
      .add( StructField( MONGO_ID,            LongType,   false))
      .add( StructField( schemaStopTimeMs,        LongType,   false))
      .add( StructField( schemaMeasureTime,     LongType,   false))           
      
      .add( StructField( "startDate",         StringType, false))
      .add( StructField( "endDate",           StringType, false))
      .add( StructField( "lp_id",             StringType, false))
      
      .add( StructField( "psdTotal",          DoubleType, false))
      
      .add( StructField( "psdFreqAccum10%",   DoubleType, false))
      .add( StructField( "psdFreqAccum20%",   DoubleType, false))
      .add( StructField( "psdFreqAccum30%",   DoubleType, false))
      .add( StructField( "psdFreqAccum40%",   DoubleType, false))
      .add( StructField( "psdFreqAccum50%",   DoubleType, false))
      .add( StructField( "psdFreqAccum60%",   DoubleType, false))
      .add( StructField( "psdFreqAccum70%",   DoubleType, false))
      .add( StructField( "psdFreqAccum80%",   DoubleType, false))
      .add( StructField( "psdFreqAccum90%",   DoubleType, false))
      
      .add( StructField( "psdFreqAccumRange", DoubleType, false))
      
   val dfSchema = new StructType()
      .add( StructField( schemaStartTimeMs,       LongType,   false))
      .add( StructField( schemaStopTimeMs,        LongType,   false))
      .add( StructField( schemaMeasureTime,     LongType,   false))           
      
      .add( StructField( "startDate",         StringType, false))
      .add( StructField( "endDate",           StringType, false))
      .add( StructField( "lp_id",             StringType, false))
      
      .add( StructField( "psdTotal",          DoubleType, false))
      
      .add( StructField( "psdFreqAccum10%",   DoubleType, false))
      .add( StructField( "psdFreqAccum20%",   DoubleType, false))
      .add( StructField( "psdFreqAccum30%",   DoubleType, false))
      .add( StructField( "psdFreqAccum40%",   DoubleType, false))
      .add( StructField( "psdFreqAccum50%",   DoubleType, false))
      .add( StructField( "psdFreqAccum60%",   DoubleType, false))
      .add( StructField( "psdFreqAccum70%",   DoubleType, false))
      .add( StructField( "psdFreqAccum80%",   DoubleType, false))
      .add( StructField( "psdFreqAccum90%",   DoubleType, false))
      
      .add( StructField( "psdFreqAccumRange", DoubleType, false))      
      
   private final val FIELD_ID= "_id"    //long
   
  //-------------------------------------------------------------------------
  // Code starts
  //-------------------------------------------------------------------------

   connect
      
  //-------------------------------------------------------------------------
  private def translateSequence( q: MongoDBObject ) = {
    
     //projection
     val p = MongoDBObject( schemaMeasureTime -> 0 ) 
     
     //sort
     val s =  MongoDBObject( MONGO_ID -> 1) 
       
     //execute the query+projection+sort
     collection.find( q, p )
      .sort( s )
      .map( obj => {(obj.get( MONGO_ID ).asInstanceOf[Long],                      //1   startTime
                     obj.get( schemaStopTimeMs ).asInstanceOf[Long],              //2 
                     obj.get( schemaMeasureTime ).asInstanceOf[Long],             //3
                     obj.get( "startDate" ).asInstanceOf[String],                 //4 
                     obj.get( "endDate" ).asInstanceOf[String],                   //5
                     obj.get( "lp_id" ).asInstanceOf[String],                     //6  
                     obj.get( "psdTotal" ).asInstanceOf[Double],                  //7
                     obj.get( "psdFreqAccum10%" ).asInstanceOf[Double],           //8
                     obj.get( "psdFreqAccum20%" ).asInstanceOf[Double],           //9
                     obj.get( "psdFreqAccum30%" ).asInstanceOf[Double],           //10
                     obj.get( "psdFreqAccum40%" ).asInstanceOf[Double],           //11
                     obj.get( "psdFreqAccum50%" ).asInstanceOf[Double],           //12 
                     obj.get( "psdFreqAccum60%" ).asInstanceOf[Double],           //13 
                     obj.get( "psdFreqAccum70%" ).asInstanceOf[Double],           //14
                     obj.get( "psdFreqAccum80%" ).asInstanceOf[Double],           //15
                     obj.get( "psdFreqAccum90%" ).asInstanceOf[Double],           //16
                     obj.get( "psdFreqAccumRange" ).asInstanceOf[Double]          //17            
                   )}).toList      
  }
  
  //-------------------------------------------------------------------------
  def get = translateSequence( new MongoDBObject(MONGO_ID  $gte 0 ) )    
    
  //-------------------------------------------------------------------------
  def get( start: Long, end: Long) = translateSequence( new MongoDBObject(MONGO_ID  $gte start  $lte end ) )
  
  //-------------------------------------------------------------------------
  def importCsv(s:String) = super.importCsv(s, dfSchemaWithMongoID)
    
  //-------------------------------------------------------------------------   
  def getMinMaxRangeID = {
    
     val q = new MongoDBObject("_id"  $gte 0)
     
     //projection
     val p = MongoDBObject(schemaStopTimeMs -> 0 ) ++= 
             MongoDBObject(schemaMeasureTime  -> 0 ) ++= 
             MongoDBObject("startDate" -> 0 ) ++= 
             MongoDBObject("endDate" -> 0 ) ++= 
             MongoDBObject("lp_id" -> 0 ) ++= 
             MongoDBObject("psdTotal" -> 0 ) ++= 
             MongoDBObject("psdFreqAccum10%" -> 0 ) ++= 
             MongoDBObject("psdFreqAccum20%" -> 0 ) ++= 
             MongoDBObject("psdFreqAccum30%" -> 0 ) ++= 
             MongoDBObject("psdFreqAccum40%" -> 0 ) ++= 
             MongoDBObject("psdFreqAccum50%" -> 0 ) ++= 
             MongoDBObject("psdFreqAccum60%" -> 0 ) ++= 
             MongoDBObject("psdFreqAccum70%" -> 0 ) ++= 
             MongoDBObject("psdFreqAccum80%" -> 0 ) ++= 
             MongoDBObject("psdFreqAccum90%" -> 0 ) ++= 
             MongoDBObject("psdFreqAccumRange" -> 0 )   
         
     //sort
     val sMin =  MongoDBObject( MONGO_ID -> 1)
     val sMax =  MongoDBObject( MONGO_ID -> -1)
            
    //execute the query+projection+sort
    (collection.findOne( q, p, sMin ).get( MONGO_ID ).asInstanceOf[Long] , 
     collection.findOne( q, p, sMax ).get( MONGO_ID ).asInstanceOf[Long])      
  }
 
  //-------------------------------------------------------------------------  
  def save(t: List[Any], extraData: List[Any]){
        
    try {                       
      val builder = collection.initializeUnorderedBulkOperation               
      builder.insert( 
        MongoDBObject(MONGO_ID ->             t(0).asInstanceOf[Long]) ++=   //startTime
        MongoDBObject(schemaStopTimeMs ->     extraData(0).asInstanceOf[Long]) ++=   //2 
        MongoDBObject(schemaMeasureTime ->    extraData(1).asInstanceOf[Long]) ++=   //3
        MongoDBObject("startDate" ->          extraData(2).asInstanceOf[String]) ++=   //4 
        MongoDBObject("endDate" ->            extraData(3).asInstanceOf[String]) ++=   //5
        MongoDBObject("lp_id" ->              t(1).asInstanceOf[String]) ++=   //6  
        MongoDBObject("psdTotal" ->           t(2).asInstanceOf[Double]) ++=   //7
        MongoDBObject("psdFreqAccum10%" ->    t(3).asInstanceOf[Double]) ++=   //8
        MongoDBObject("psdFreqAccum20%"  ->   t(4).asInstanceOf[Double]) ++=   //9
        MongoDBObject("psdFreqAccum30%" ->    t(5).asInstanceOf[Double]) ++=  //10
        MongoDBObject("psdFreqAccum40%" ->    t(6).asInstanceOf[Double]) ++=  //11
        MongoDBObject("psdFreqAccum50%" ->    t(7).asInstanceOf[Double]) ++=  //12 
        MongoDBObject("psdFreqAccum60%" ->    t(8).asInstanceOf[Double]) ++=  //13 
        MongoDBObject("psdFreqAccum70%" ->    t(9).asInstanceOf[Double]) ++=  //14
        MongoDBObject("psdFreqAccum80%" ->    t(10).asInstanceOf[Double]) ++= //15
        MongoDBObject("psdFreqAccum90%" ->    t(11).asInstanceOf[Double]) ++= //16
        MongoDBObject("psdFreqAccumRange" ->  t(12).asInstanceOf[Double]))    //17)
    
       //insert in batch mode
       if (!builder.execute.isAcknowledged) log.error(s"Error importing data into mongoDB")          
    } 
    catch {
      case e:Exception => log.error(e.toString)
    } 
  }
  
  
  //-------------------------------------------------------------------------
  def renameFieldList(df: DataFrame) = df.toDF( Seq("startTimeMs"
    ,  schemaStopTimeMs
    ,  schemaMeasureTime
    ,  "startDate"
    ,  "endDate"
    ,  "lp_id"
    ,  "psdTotal"
    ,  "psdFreqAccum10%"
    ,  "psdFreqAccum20%"
    ,  "psdFreqAccum30%"
    ,  "psdFreqAccum40%"
    ,  "psdFreqAccum50%"
    ,  "psdFreqAccum60%"
    ,  "psdFreqAccum70%"
    ,  "psdFreqAccum80%"
    ,  "psdFreqAccum90%"
    ,  "psdFreqAccumRange"): _*)
  
  //-------------------------------------------------------------------------
  def loadDF = renameFieldList(super.loadDF( dfSchemaWithMongoID ))      
  
  //-------------------------------------------------------------------------
  private def getList( r: MongoCursor ) = {
    
    r.map { obj => 
      
      val t0 = obj.get(MONGO_ID).asInstanceOf[Long]
      val t1 = obj.get(schemaStopTimeMs).asInstanceOf[Long]
      val t2 = obj.get(schemaMeasureTime).asInstanceOf[Long]
      val t3 = obj.get("startDate").asInstanceOf[String]
      val t4 = obj.get("endDate").asInstanceOf[String]
      val t5 = obj.get("lp_id").asInstanceOf[String]
      val t6 = obj.get("psdTotal").asInstanceOf[Double]
      val t7 = obj.get("psdFreqAccum10%").asInstanceOf[Double]
      val t8 = obj.get("psdFreqAccum20%").asInstanceOf[Double]
      val t9 = obj.get("psdFreqAccum30%").asInstanceOf[Double]
      val t10 = obj.get("psdFreqAccum40%").asInstanceOf[Double]
      val t11 = obj.get("psdFreqAccum50%").asInstanceOf[Double] 
      val t12 = obj.get("psdFreqAccum60%").asInstanceOf[Double]
      val t13 = obj.get("psdFreqAccum70%").asInstanceOf[Double]
      val t14 = obj.get("psdFreqAccum80%").asInstanceOf[Double]
      val t15 = obj.get("psdFreqAccum90%").asInstanceOf[Double]
      val t16 = obj.get("psdFreqAccumRange").asInstanceOf[Double]
      
      (t0,t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11,t12,t13,t14,t15,t16)
      
      }.toList         
  }
    
    //-------------------------------------------------------------------------
  def getDataFrame(start: Long, end: Long) = {
    
     val q = new MongoDBObject(MONGO_ID  $gte start  $lte end )
         
     //sort
     val s =  MongoDBObject( MONGO_ID -> 1) 
       
     //execute the query+sort
     getDadataFrame(collection.find( q ).sort( s ))     
  }
  //-------------------------------------------------------------------------  
  private def getDadataFrame( r: MongoCursor ) = {
    
    val l = getList(r) 
    
    sparkSession.createDataFrame(sparkSession.sparkContext.parallelize( l ).map { w => Row( w._1, w._2, w._3, w._4, w._5, w._6, w._7, w._8, w._9, w._10, w._11, w._12, w._13, w._14, w._15, w._16, w._17 ) }    
      , dfSchema )       
  }
     

  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
} //end of class 'PsdMDB'
//=============================================================================
// End of 'psdMDB.scala' file
//=============================================================================
