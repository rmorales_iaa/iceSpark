//=============================================================================
//File: tideMDB.scala
//=============================================================================
/** It implements a MongoDB client for tide data 
 *  @author  Rafael Morales Muñoz
 *  @mail    rmorales.iaa.es
 *  @version 1.0
 *  @date    10 Dec 2017
 *  @history None
 */
//=============================================================================
//=============================================================================
// Package section
//=============================================================================
package iceSpark.mongoDB

//=============================================================================
// System import section
//=============================================================================
import com.mongodb.casbah.{ MongoCollection }
import com.mongodb.casbah.Imports._

import org.apache.spark.sql.types._

import org.apache.spark.sql.{ DataFrame, Row, SQLContext, SparkSession }
  
//=============================================================================
// User import section
//=============================================================================
import catSpark.logger.visual.LoggerVisual
import catSpark.database.mongoDB.MyMongoDB
import catSpark.spark.dataFrame.MyDataframe
import catSpark.util.MyUtil

//=============================================================================
// Class/Object implementation
//=============================================================================
import MyMongoDB._

class TideMDB( dbName: String, collectionName: String ) extends MyMongoDB( dbName, collectionName ) {
  
  //-------------------------------------------------------------------------
  // Constants
  //-------------------------------------------------------------------------
  
   val mongoConfiguration = getConfiguration
        
   private final val MONGO_ID= "_id"    //long
   private final val TIDE_TIME_BETWEEN_MESURES_MS : Long = (15 * 60 * 1000)   
  
  //-------------------------------------------------------------------------
  // Code starts
  //-------------------------------------------------------------------------

   connect
      
  //-------------------------------------------------------------------------
  private def translateSequence( q: MongoDBObject ) = {
     
     //sort
     val s =  MongoDBObject( MONGO_ID -> 1) 
       
     //execute the query+projection+sort
     collection.find( q )
      .sort( s )
      .map( obj => {(obj.get( MONGO_ID ).asInstanceOf[Long],
                     obj.get( "height[m]" ).asInstanceOf[Double]
                   )}).toList      
  }
  
  //-------------------------------------------------------------------------
  def get = translateSequence( new MongoDBObject(MONGO_ID  $gte 0 ) )    
    
  //-------------------------------------------------------------------------
  def get( start: Long, end: Long) = {
    
    val newEnd = end - TIDE_TIME_BETWEEN_MESURES_MS
     
    translateSequence(new MongoDBObject(MONGO_ID $lte start $gte newEnd ))
  }
  
  //-------------------------------------------------------------------------   
  
  //-------------------------------------------------------------------------
  //-------------------------------------------------------------------------
} //end of class 'TideMDB'
//=============================================================================
// End of 'tideMDB.scala' file
//=============================================================================
